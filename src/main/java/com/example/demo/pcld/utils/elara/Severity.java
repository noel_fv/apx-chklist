package com.example.demo.pcld.utils.elara;


public  enum Severity {
    OK("00"),


    WARN("04"),


    ENR("06"),


    EWR("08"),


    ERROR("12"),


    BANKING_ENR("16"),


    BANKING_EWR("18");


    private String value;


    Severity(String value) {
        this.value = value;
    }


    public String getValue() {
        return this.value;
    }
}
