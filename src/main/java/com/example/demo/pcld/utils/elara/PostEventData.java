package com.example.demo.pcld.utils.elara;

import java.io.Serializable;
import java.util.Objects;

public class PostEventData
        implements Serializable {
    private static final long serialVersionUID = 1L;
    private String namespace;
    private String stream;
    private String jsonBody;

    public static Builder newBuilder() {
        return new Builder("");
    }


    public static class Builder {
        private PostEventData postEventData = new PostEventData();


        public Builder addNamespace(String namespace) {
            this.postEventData.setNamespace(namespace);
            return this;
        }

        public Builder addStream(String stream) {
            this.postEventData.setStream(stream);
            return this;
        }

        public Builder addJsonBody(String jsonBody) {
            this.postEventData.setJsonBody(jsonBody);
            return this;
        }


        public PostEventData build() {
            return this.postEventData;
        }

        private Builder(String s) {
        }
    }

    private void setNamespace(String namespace) {
        this.namespace = namespace;
    }


    private void setJsonBody(String jsonBody) {
        this.jsonBody = jsonBody;
    }


    public String getNamespace() {
        return this.namespace;
    }


    public String getJsonBody() {
        return this.jsonBody;
    }


    public String getStream() {
        return this.stream;
    }


    public void setStream(String stream) {
        this.stream = stream;
    }


    public String toString() {
        return "PostEventData{namespace='" + this.namespace + '\'' + ", stream='" + this.stream + '\'' + ", jsonBody='" + this.jsonBody + '\'' + '}';
    }
}
