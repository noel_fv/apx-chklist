package com.example.demo.pcld.utils.elara;


//import com.bbva.elara.domain.transaction.event.PostEventData;
//import com.bbva.elara.domain.transaction.request.TransactionRequest;
//import com.bbva.elara.domain.xsd.mapping.Transaction;

import java.io.Serializable;
import java.sql.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Map;

import org.apache.cxf.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Context
        implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LoggerFactory.getLogger(Context.class);


    private Map<String, TransactionParameter> parameterList;


    private ContextStatusType endStatus;


    private Severity severity;


    private LinkedList<Advice> adviceList;


    private RopInfo ropInfo;


    private LinkedList<VariableRopInfo> variableRopInfoList;


    private LinkedList<LotesRopInfo> lotesRopInfoList;


    private LinkedList<JournalData> journalDataList;


    private LinkedList<EventData> eventDataList;


    private LinkedList<ExternalData> externalDataList;


    private PostEventData postEventData;


    private Transaction transaction;


    private String languageCode;


    private ArchitectureReturnCode architecturRollbackRequired;


    private TransactionRequest transactionRequest;


    private Object genericResponse;


    private boolean proxiedResponse;


    private Date accountingDateCurrentDate;


    private Date accountingDatePreviousDate;


    private Date accountingDateNextDate;


    private AsyncTransactionInvokerInfo asyncTransactionInvokerInfo;


    private Map<String, String> dynamicSqls;


    private Message httpMessageResponse;


    private long initialTime;


    private String jobName;


    private String jobID;


    private boolean remainingTimeEnabled;


    private String libraryName;


    public Context() {
        this.parameterList = new Hashtable();
        this.adviceList = new LinkedList();
        this.ropInfo = null;
        this.variableRopInfoList = new LinkedList();
        this.lotesRopInfoList = new LinkedList();
        this.journalDataList = new LinkedList();
        this.eventDataList = new LinkedList();
        this.externalDataList = new LinkedList();
        this.postEventData = null;
        this.transaction = null;
        this.endStatus = ContextStatusType.OK;
        this.languageCode = "";
        this.severity = Severity.OK;
        this.architecturRollbackRequired = null;
        this.accountingDateCurrentDate = null;
        this.accountingDateNextDate = null;
        this.accountingDatePreviousDate = null;
        setAsyncTransactionInvokerInfo(null);
        this.dynamicSqls = new HashMap();
        this.initialTime = 0L;
        this.jobName = null;
        this.jobID = null;
    }


    public Map<String, TransactionParameter> getParameterList() {
        LOGGER.debug("Got parameterList: {}", this.parameterList);
        return this.parameterList;
    }


    public void addParameter(String key, TransactionParameter value) {
        this.parameterList.put(key, value);
        LOGGER.debug("Added parameter: {}=>{}", key, value);
        LOGGER.debug("Parameters are: {}", this.parameterList);
    }


    public void addParameters(Map<String, TransactionParameter> values) {
        this.parameterList.putAll(values);
        LOGGER.debug("Added parameters: {}", values);
    }


    public void setParameterList(Map<String, TransactionParameter> parameterList) {
        LOGGER.debug("Setted parameterList: {}", parameterList);
        this.parameterList = parameterList;
    }


    public ContextStatusType getEndStatus() {
        return this.endStatus;
    }


    public void setEndStatus(ContextStatusType endStatus) {
        this.endStatus = endStatus;
    }


    public Severity getSeverity() {
        return this.severity;
    }


    public void setSeverity(Severity severity) {
        this.severity = severity;
    }


    public LinkedList<Advice> getAdviceList() {
        return this.adviceList;
    }


    public void setAdviceList(LinkedList<Advice> adviceList) {
        this.adviceList = adviceList;
    }


    public RopInfo getRopInfo() {
        return this.ropInfo;
    }


    public void setRopInfo(RopInfo ropInfo) {
        this.ropInfo = ropInfo;
    }


    public LinkedList<VariableRopInfo> getVariableRopInfoList() {
        return this.variableRopInfoList;
    }


    public void setVariableRopInfoList(LinkedList<VariableRopInfo> variableRopInfoList) {
        this.variableRopInfoList = variableRopInfoList;
    }


    public LinkedList<LotesRopInfo> getLotesRopInfoList() {
        return this.lotesRopInfoList;
    }


    public void setLotesRopInfoList(LinkedList<LotesRopInfo> lotesRopInfoList) {
        this.lotesRopInfoList = lotesRopInfoList;
    }


    public LinkedList<JournalData> getJournalDataList() {
        return this.journalDataList;
    }


    public void setJournalDataList(LinkedList<JournalData> journalDataList) {
        this.journalDataList = journalDataList;
    }


    public LinkedList<EventData> getEventDataList() {
        return this.eventDataList;
    }


    public void setEventDataList(LinkedList<EventData> eventDataList) {
        this.eventDataList = eventDataList;
    }


    public LinkedList<ExternalData> getExternalDataList() {
        return this.externalDataList;
    }


    public void setExternalDataList(LinkedList<ExternalData> externalDataList) {
        this.externalDataList = externalDataList;
    }


    public Transaction getTransaction() {
        return this.transaction;
    }


    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }


    public final String getLanguageCode() {
        return this.languageCode;
    }


    public final void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }


    public final TransactionRequest getTransactionRequest() {
        return this.transactionRequest;
    }


    public final void setTransactionRequest(TransactionRequest transactionRequest) {
        this.transactionRequest = transactionRequest;
        for (TransactionParameter transactionParameterRequest : transactionRequest.getBody().getTransactionParameters()) {
            TransactionParameter transParameter = new TransactionParameter();
            String trxParameterName = transactionParameterRequest.getName();
            transParameter.setName(trxParameterName);
            transParameter.setValue(transactionParameterRequest.getValue());
            this.parameterList.put(trxParameterName, transParameter);
        }
    }


    public final Object getGenericResponse() {
        return this.genericResponse;
    }


    public final void setGenericResponse(Object genericResponse) {
        this.genericResponse = genericResponse;
    }


    public final boolean isProxiedResponse() {
        return this.proxiedResponse;
    }


    public final void setProxiedResponse(boolean proxiedResponse) {
        this.proxiedResponse = proxiedResponse;
    }


    public ArchitectureReturnCode getArchitecturRollbackRequired() {
        return this.architecturRollbackRequired;
    }


    public void setArchitecturRollbackRequired(ArchitectureReturnCode architecturRollbackRequired) {
        this.architecturRollbackRequired = architecturRollbackRequired;
    }


    public Date getAccountingDateCurrentDate() {
        return this.accountingDateCurrentDate;
    }


    public void setAccountingDateCurrentDate(Date accountingDateCurrentDate) {
        this.accountingDateCurrentDate = accountingDateCurrentDate;
    }


    public Date getAccountingDatePreviousDate() {
        return this.accountingDatePreviousDate;
    }


    public void setAccountingDatePreviousDate(Date accountingDatePreviousDate) {
        this.accountingDatePreviousDate = accountingDatePreviousDate;
    }


    public Date getAccountingDateNextDate() {
        return this.accountingDateNextDate;
    }


    public void setAccountingDateNextDate(Date accountingDateNextDate) {
        this.accountingDateNextDate = accountingDateNextDate;
    }


    public AsyncTransactionInvokerInfo getAsyncTransactionInvokerInfo() {
        return this.asyncTransactionInvokerInfo;
    }


    public void setAsyncTransactionInvokerInfo(AsyncTransactionInvokerInfo asyncTransactionInvokerInfo) {
        this.asyncTransactionInvokerInfo = asyncTransactionInvokerInfo;
    }


    public Map<String, String> getDynamicSqls() {
        if (this.dynamicSqls == null) {
            this.dynamicSqls = new HashMap();
        }
        return this.dynamicSqls;
    }


    public void addDynamicSql(String code, String sql) {
        if (this.dynamicSqls == null) {
            this.dynamicSqls = new HashMap();
        }
        this.dynamicSqls.put(code, sql);
    }


    public String toString() {
        return "Context [parameterList=" + this.parameterList + ", endStatus=" + this.endStatus + ", severity=" + this.severity + ", adviceList=" + this.adviceList + ", ropInfo=" + this.ropInfo + ", variableRopInfoList=" + this.variableRopInfoList + ", lotesRopInfoList=" + this.lotesRopInfoList + ", journalDataList=" + this.journalDataList + ", eventDataList=" + this.eventDataList + ", externalDataList=" + this.externalDataList + ", postEventData=" + this.postEventData + ", transaction=" + this.transaction + ", languageCode=" + this.languageCode + ", architecturRollbackRequired=" + this.architecturRollbackRequired + ", transactionRequest=" + this.transactionRequest + ", genericResponse=" + this.genericResponse + ", proxiedResponse=" + this.proxiedResponse + ", accountingDateCurrentDate=" + this.accountingDateCurrentDate + ", accountingDatePreviousDate=" + this.accountingDatePreviousDate + ", accountingDateNextDate=" + this.accountingDateNextDate + ", asyncTransactionInvokerInfo=" + this.asyncTransactionInvokerInfo + "]";
    }


    public Message getHttpMessageResponse() {
        return this.httpMessageResponse;
    }


    public void setHttpMessageResponse(Message httpMessageResponse) {
        this.httpMessageResponse = httpMessageResponse;
    }


    public long getInitialTime() {
        return this.initialTime;
    }


    public void setInitialTime(long initialTime) {
        this.initialTime = initialTime;
    }


    public String getJobName() {
        return this.jobName;
    }


    public void setJobName(String jobName) {
        this.jobName = jobName;
    }


    public String getJobID() {
        return this.jobID;
    }


    public void setJobID(String jobID) {
        this.jobID = jobID;
    }


    public boolean isRemainingTimeEnabled() {
        return this.remainingTimeEnabled;
    }


    public void setRemainingTimeEnabled(boolean remainingTimeEnabled) {
        this.remainingTimeEnabled = remainingTimeEnabled;
    }


    public String getLibraryName() {
        return this.libraryName;
    }


    public void setLibraryName(String libraryName) {
        this.libraryName = libraryName;
    }


    public PostEventData getPostEventData() {
        return this.postEventData;
    }


    public void setPostEventData(PostEventData postEventData) {
        this.postEventData = postEventData;
    }
}

