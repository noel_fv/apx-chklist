package com.example.demo.pcld.utils.elara;


import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;































































































































































































































































































































































































































public class RopInfo
        implements Serializable
{
    private static final long serialVersionUID = 1L;
    private String ibanOrigin;
    private String origAccountOperation;
    private String origCountryAccount;
    private String ibanDestiny;
    private String destAccountOperation;
    private String destCountryAccount;
    private String customerType;
    private String customerDocument;
    private Integer relatedUid;
    private String currencyCode;
    private BigDecimal amount;
    private String referenceType1;
    private String reference1;
    private String referenceType2;
    private String reference2;
    private Date operationDate;
    private String totalCode1;
    private String currency1;
    private String indDeHa1;
    private BigDecimal totalAmmount1;
    private String totalCode2;
    private String currency2;
    private String indDeHa2;
    private BigDecimal totalAmmount2;
    private BigDecimal rmsCode;
    private String currencyOriginalCode;
    private BigDecimal totalAmmountRms;
    private String interestRmsCode;
    private BigDecimal totalInterestRms;
    private String commisionRmsCode;
    private BigDecimal totalCommisionRms;
    private String expensesRmsCode;
    private BigDecimal totalExpensesRms;
    private String programCode;
    private String customerCode;
    private String additionalInfo;
    private String totalCode3;
    private String currency3;
    private String indDeHa3;
    private BigDecimal totalAmmount3;
    private String totalCode4;
    private String currency4;
    private String indDeHa4;
    private BigDecimal totalAmmount4;
    private Short numLoteDetails;
    private String signatureInd;
    private String appCode;
    private String operationCode;
    private String docCode;
    private BigDecimal amountMIS;
    private String contractEntity1;
    private String contractBranch1;
    private String contractContrapart1;
    private String contractfolio1;
    private String sequenceId1;
    private String currencyOperation;
    private String appReferenceType;
    private String appReferenceDesc;
    private String appReferenceCode;
    private Integer detailRemittancesNumber;
    private String operationType1;
    private String processDate;
    private String takerEntity;
    private String takerBranch;
    private String uniqueContractCode1;
    private String contractEntity2;
    private String contractBranch2;
    private String contractContrapart2;
    private String contractfolio2;
    private String sequenceId2;
    private String uniqueContractCode2;
    private String operationType2;
    private String commerceCode;
    private String contractType;
    private String card;
    private String contractCardCode;
    private String contractDetail1;
    private String contractDetail2;
    private String clientCode;
    private String clientIdentifying;
    private String xtiClientIdentifying;
    private String countryCode;
    private String channelCode;
    private String subChanelCode;
    private String errorCode;
    private String severityError;
    private String serviceCode;
    private Date channelDate;
    private Timestamp channelTime;
    private BigDecimal comisrui;
    private String channelAp;
    private String environmentAp;
    private String serviceAp;

    public RopInfo() {}

    @Deprecated
    public RopInfo(String ibanOrigin, String origAccountOperation, String origCountryAccount, String ibanDestiny, String destAccountOperation, String destCountryAccount, String customerType, String customerDocument, Integer relatedUid, String currencyCode, BigDecimal amount, String referenceType1, String reference1, String referenceType2, String reference2, Date operationDate, String totalCode1, String currency1, String indDeHa1, BigDecimal totalAmmount1, String totalCode2, String currency2, String indDeHa2, BigDecimal totalAmmount2, BigDecimal rmsCode, String currencyOriginalCode, BigDecimal totalAmmountRms, String interestRmsCode, BigDecimal totalInterestRms, String commisionRmsCode, BigDecimal totalCommisionRms, String expensesRmsCode, BigDecimal totalExpensesRms, String programCode, String customerCode, String additionalInfo, String totalCode3, String currency3, String indDeHa3, BigDecimal totalAmmount3, String totalCode4, String currency4, String indDeHa4, BigDecimal totalAmmount4, Short numLoteDetails, String signatureInd, String appCode, String operationCode, String docCode) {
        this.ibanOrigin = ibanOrigin;
        this.origAccountOperation = origAccountOperation;
        this.origCountryAccount = origCountryAccount;
        this.ibanDestiny = ibanDestiny;
        this.destAccountOperation = destAccountOperation;
        this.destCountryAccount = destCountryAccount;
        this.customerType = customerType;
        this.customerDocument = customerDocument;
        this.relatedUid = relatedUid;
        this.currencyCode = currencyCode;
        this.amount = amount;
        this.referenceType1 = referenceType1;
        this.reference1 = reference1;
        this.referenceType2 = referenceType2;
        this.reference2 = reference2;
        this.operationDate = operationDate;
        this.totalCode1 = totalCode1;
        this.currency1 = currency1;
        this.indDeHa1 = indDeHa1;
        this.totalAmmount1 = totalAmmount1;
        this.totalCode2 = totalCode2;
        this.currency2 = currency2;
        this.indDeHa2 = indDeHa2;
        this.totalAmmount2 = totalAmmount2;
        this.rmsCode = rmsCode;
        this.currencyOriginalCode = currencyOriginalCode;
        this.totalAmmountRms = totalAmmountRms;
        this.interestRmsCode = interestRmsCode;
        this.totalInterestRms = totalInterestRms;
        this.commisionRmsCode = commisionRmsCode;
        this.totalCommisionRms = totalCommisionRms;
        this.expensesRmsCode = expensesRmsCode;
        this.totalExpensesRms = totalExpensesRms;
        this.programCode = programCode;
        this.customerCode = customerCode;
        this.additionalInfo = additionalInfo;
        this.totalCode3 = totalCode3;
        this.currency3 = currency3;
        this.indDeHa3 = indDeHa3;
        this.totalAmmount3 = totalAmmount3;
        this.totalCode4 = totalCode4;
        this.currency4 = currency4;
        this.indDeHa4 = indDeHa4;
        this.totalAmmount4 = totalAmmount4;
        this.numLoteDetails = numLoteDetails;
        this.signatureInd = signatureInd;
        this.appCode = appCode;
        this.operationCode = operationCode;
        this.docCode = docCode;
    }



































































































































































    public RopInfo(String ibanOrigin, String origAccountOperation, String origCountryAccount, String ibanDestiny, String destAccountOperation, String destCountryAccount, String customerType, String customerDocument, Integer relatedUid, String currencyCode, BigDecimal amount, String referenceType1, String reference1, String referenceType2, String reference2, Date operationDate, String totalCode1, String currency1, String indDeHa1, BigDecimal totalAmmount1, String totalCode2, String currency2, String indDeHa2, BigDecimal totalAmmount2, BigDecimal rmsCode, String currencyOriginalCode, BigDecimal totalAmmountRms, String interestRmsCode, BigDecimal totalInterestRms, String commisionRmsCode, BigDecimal totalCommisionRms, String expensesRmsCode, BigDecimal totalExpensesRms, String programCode, String customerCode, String additionalInfo, String totalCode3, String currency3, String indDeHa3, BigDecimal totalAmmount3, String totalCode4, String currency4, String indDeHa4, BigDecimal totalAmmount4, Short numLoteDetails, String signatureInd, String appCode, String operationCode, String docCode, BigDecimal amountMIS, String contractEntity1, String contractBranch1, String contractContrapart1, String contractfolio1, String sequenceId1, String currencyOperation, String appReferenceType, String appReferenceDesc, String appReferenceCode, Integer detailRemittancesNumber, String operationType1, String processDate, String takerEntity, String takerBranch, String uniqueContractCode1, String contractEntity2, String contractBranch2, String contractContrapart2, String contractfolio2, String sequenceId2, String uniqueContractCode2, String operationType2, String commerceCode, String contractType, String card, String contractCardCode, String contractDetail1, String contractDetail2, String clientCode, String clientIdentifying, String xtiClientIdentifying, String countryCode, String channelCode, String subChanelCode, String errorCode, String severityError, String serviceCode, Date channelDate, Timestamp channelTime, BigDecimal comisrui, String channelAp, String environmentAp, String serviceAp) {
        this.ibanOrigin = ibanOrigin;
        this.origAccountOperation = origAccountOperation;
        this.origCountryAccount = origCountryAccount;
        this.ibanDestiny = ibanDestiny;
        this.destAccountOperation = destAccountOperation;
        this.destCountryAccount = destCountryAccount;
        this.customerType = customerType;
        this.customerDocument = customerDocument;
        this.relatedUid = relatedUid;
        this.currencyCode = currencyCode;
        this.amount = amount;
        this.referenceType1 = referenceType1;
        this.reference1 = reference1;
        this.referenceType2 = referenceType2;
        this.reference2 = reference2;
        this.operationDate = operationDate;
        this.totalCode1 = totalCode1;
        this.currency1 = currency1;
        this.indDeHa1 = indDeHa1;
        this.totalAmmount1 = totalAmmount1;
        this.totalCode2 = totalCode2;
        this.currency2 = currency2;
        this.indDeHa2 = indDeHa2;
        this.totalAmmount2 = totalAmmount2;
        this.rmsCode = rmsCode;
        this.currencyOriginalCode = currencyOriginalCode;
        this.totalAmmountRms = totalAmmountRms;
        this.interestRmsCode = interestRmsCode;
        this.totalInterestRms = totalInterestRms;
        this.commisionRmsCode = commisionRmsCode;
        this.totalCommisionRms = totalCommisionRms;
        this.expensesRmsCode = expensesRmsCode;
        this.totalExpensesRms = totalExpensesRms;
        this.programCode = programCode;
        this.customerCode = customerCode;
        this.additionalInfo = additionalInfo;
        this.totalCode3 = totalCode3;
        this.currency3 = currency3;
        this.indDeHa3 = indDeHa3;
        this.totalAmmount3 = totalAmmount3;
        this.totalCode4 = totalCode4;
        this.currency4 = currency4;
        this.indDeHa4 = indDeHa4;
        this.totalAmmount4 = totalAmmount4;
        this.numLoteDetails = numLoteDetails;
        this.signatureInd = signatureInd;
        this.appCode = appCode;
        this.operationCode = operationCode;
        this.docCode = docCode;


        this.amountMIS = amountMIS;
        this.contractEntity1 = contractEntity1;
        this.contractBranch1 = contractBranch1;
        this.contractContrapart1 = contractContrapart1;
        this.contractfolio1 = contractfolio1;
        this.sequenceId1 = sequenceId1;
        this.currencyOperation = currencyOperation;
        this.appReferenceType = appReferenceType;
        this.appReferenceDesc = appReferenceDesc;
        this.appReferenceCode = appReferenceCode;
        this.detailRemittancesNumber = detailRemittancesNumber;
        this.operationType1 = operationType1;
        this.processDate = processDate;
        this.takerEntity = takerEntity;
        this.takerBranch = takerBranch;
        this.uniqueContractCode1 = uniqueContractCode1;
        this.contractEntity2 = contractEntity2;
        this.contractBranch2 = contractBranch2;
        this.contractContrapart2 = contractContrapart2;
        this.contractfolio2 = contractfolio2;
        this.sequenceId2 = sequenceId2;
        this.uniqueContractCode2 = uniqueContractCode2;
        this.operationType2 = operationType2;
        this.commerceCode = commerceCode;
        this.contractType = contractType;
        this.card = card;
        this.contractCardCode = contractCardCode;
        this.contractDetail1 = contractDetail1;
        this.contractDetail2 = contractDetail2;
        this.clientCode = clientCode;
        this.clientIdentifying = clientIdentifying;
        this.xtiClientIdentifying = xtiClientIdentifying;
        this.countryCode = countryCode;
        this.channelCode = channelCode;
        this.subChanelCode = subChanelCode;
        this.errorCode = errorCode;
        this.severityError = severityError;
        this.serviceCode = serviceCode;
        this.channelDate = channelDate;
        this.channelTime = channelTime;
        this.comisrui = comisrui;
        this.channelAp = channelAp;
        this.environmentAp = environmentAp;
        this.serviceAp = serviceAp;
    }








    public String getIbanOrigin() { return this.ibanOrigin; }









    public void setIbanOrigin(String ibanOrigin) { this.ibanOrigin = ibanOrigin; }








    public String getOrigAccountOperation() { return this.origAccountOperation; }









    public void setOrigAccountOperation(String origAccountOperation) { this.origAccountOperation = origAccountOperation; }








    public String getOrigCountryAccount() { return this.origCountryAccount; }









    public void setOrigCountryAccount(String origCountryAccount) { this.origCountryAccount = origCountryAccount; }








    public String getIbanDestiny() { return this.ibanDestiny; }









    public void setIbanDestiny(String ibanDestiny) { this.ibanDestiny = ibanDestiny; }








    public String getDestAccountOperation() { return this.destAccountOperation; }









    public void setDestAccountOperation(String destAccountOperation) { this.destAccountOperation = destAccountOperation; }








    public String getDestCountryAccount() { return this.destCountryAccount; }









    public void setDestCountryAccount(String destCountryAccount) { this.destCountryAccount = destCountryAccount; }








    public String getCustomerType() { return this.customerType; }









    public void setCustomerType(String customerType) { this.customerType = customerType; }








    public String getCustomerDocument() { return this.customerDocument; }









    public void setCustomerDocument(String customerDocument) { this.customerDocument = customerDocument; }








    public Integer getRelatedUid() { return this.relatedUid; }









    public void setRelatedUid(Integer relatedUid) { this.relatedUid = relatedUid; }








    public String getCurrencyCode() { return this.currencyCode; }









    public void setCurrencyCode(String currencyCode) { this.currencyCode = currencyCode; }








    public BigDecimal getAmount() { return this.amount; }









    public void setAmount(BigDecimal amount) { this.amount = amount; }








    public String getReferenceType1() { return this.referenceType1; }









    public void setReferenceType1(String referenceType1) { this.referenceType1 = referenceType1; }








    public String getReference1() { return this.reference1; }









    public void setReference1(String reference1) { this.reference1 = reference1; }








    public String getReferenceType2() { return this.referenceType2; }









    public void setReferenceType2(String referenceType2) { this.referenceType2 = referenceType2; }








    public String getReference2() { return this.reference2; }









    public void setReference2(String reference2) { this.reference2 = reference2; }








    public Date getOperationDate() { return this.operationDate; }









    public void setOperationDate(Date operationDate) { this.operationDate = operationDate; }








    public String getTotalCode1() { return this.totalCode1; }









    public void setTotalCode1(String totalCode1) { this.totalCode1 = totalCode1; }








    public String getCurrency1() { return this.currency1; }









    public void setCurrency1(String currency1) { this.currency1 = currency1; }








    public String getIndDeHa1() { return this.indDeHa1; }









    public void setIndDeHa1(String indDeHa1) { this.indDeHa1 = indDeHa1; }








    public BigDecimal getTotalAmmount1() { return this.totalAmmount1; }









    public void setTotalAmmount1(BigDecimal totalAmmount1) { this.totalAmmount1 = totalAmmount1; }








    public String getTotalCode2() { return this.totalCode2; }









    public void setTotalCode2(String totalCode2) { this.totalCode2 = totalCode2; }








    public String getCurrency2() { return this.currency2; }









    public void setCurrency2(String currency2) { this.currency2 = currency2; }








    public String getIndDeHa2() { return this.indDeHa2; }









    public void setIndDeHa2(String indDeHa2) { this.indDeHa2 = indDeHa2; }








    public BigDecimal getTotalAmmount2() { return this.totalAmmount2; }









    public void setTotalAmmount2(BigDecimal totalAmmount2) { this.totalAmmount2 = totalAmmount2; }








    public BigDecimal getRmsCode() { return this.rmsCode; }









    public void setRmsCode(BigDecimal rmsCode) { this.rmsCode = rmsCode; }








    public String getCurrencyOriginalCode() { return this.currencyOriginalCode; }









    public void setCurrencyOriginalCode(String currencyOriginalCode) { this.currencyOriginalCode = currencyOriginalCode; }








    public BigDecimal getTotalAmmountRms() { return this.totalAmmountRms; }









    public void setTotalAmmountRms(BigDecimal totalAmmountRms) { this.totalAmmountRms = totalAmmountRms; }








    public String getInterestRmsCode() { return this.interestRmsCode; }









    public void setInterestRmsCode(String interestRmsCode) { this.interestRmsCode = interestRmsCode; }








    public BigDecimal getTotalInterestRms() { return this.totalInterestRms; }









    public void setTotalInterestRms(BigDecimal totalInterestRms) { this.totalInterestRms = totalInterestRms; }








    public String getCommisionRmsCode() { return this.commisionRmsCode; }









    public void setCommisionRmsCode(String commisionRmsCode) { this.commisionRmsCode = commisionRmsCode; }








    public BigDecimal getTotalCommisionRms() { return this.totalCommisionRms; }









    public void setTotalCommisionRms(BigDecimal totalCommisionRms) { this.totalCommisionRms = totalCommisionRms; }








    public String getExpensesRmsCode() { return this.expensesRmsCode; }









    public void setExpensesRmsCode(String expensesRmsCode) { this.expensesRmsCode = expensesRmsCode; }








    public BigDecimal getTotalExpensesRms() { return this.totalExpensesRms; }









    public void setTotalExpensesRms(BigDecimal totalExpensesRms) { this.totalExpensesRms = totalExpensesRms; }








    public String getProgramCode() { return this.programCode; }









    public void setProgramCode(String programCode) { this.programCode = programCode; }








    public String getCustomerCode() { return this.customerCode; }









    public void setCustomerCode(String customerCode) { this.customerCode = customerCode; }








    public String getAdditionalInfo() { return this.additionalInfo; }









    public void setAdditionalInfo(String additionalInfo) { this.additionalInfo = additionalInfo; }








    public String getTotalCode3() { return this.totalCode3; }









    public void setTotalCode3(String totalCode3) { this.totalCode3 = totalCode3; }








    public String getCurrency3() { return this.currency3; }









    public void setCurrency3(String currency3) { this.currency3 = currency3; }








    public String getIndDeHa3() { return this.indDeHa3; }









    public void setIndDeHa3(String indDeHa3) { this.indDeHa3 = indDeHa3; }








    public BigDecimal getTotalAmmount3() { return this.totalAmmount3; }









    public void setTotalAmmount3(BigDecimal totalAmmount3) { this.totalAmmount3 = totalAmmount3; }








    public String getTotalCode4() { return this.totalCode4; }









    public void setTotalCode4(String totalCode4) { this.totalCode4 = totalCode4; }








    public String getCurrency4() { return this.currency4; }









    public void setCurrency4(String currency4) { this.currency4 = currency4; }








    public String getIndDeHa4() { return this.indDeHa4; }









    public void setIndDeHa4(String indDeHa4) { this.indDeHa4 = indDeHa4; }








    public BigDecimal getTotalAmmount4() { return this.totalAmmount4; }









    public void setTotalAmmount4(BigDecimal totalAmmount4) { this.totalAmmount4 = totalAmmount4; }








    public Short getNumLoteDetails() { return this.numLoteDetails; }









    public void setNumLoteDetails(Short numLoteDetails) { this.numLoteDetails = numLoteDetails; }








    public String getSignatureInd() { return this.signatureInd; }









    public void setSignatureInd(String signatureInd) { this.signatureInd = signatureInd; }








    public String getAppCode() { return this.appCode; }









    public void setAppCode(String appCode) { this.appCode = appCode; }








    public String getOperationCode() { return this.operationCode; }









    public void setOperationCode(String operationCode) { this.operationCode = operationCode; }








    public String getDocCode() { return this.docCode; }









    public void setDocCode(String docCode) { this.docCode = docCode; }







    public BigDecimal getAmountMIS() { return this.amountMIS; }



    public void setAmountMIS(BigDecimal amountMIS) { this.amountMIS = amountMIS; }






    public String getContractEntity1() { return this.contractEntity1; }







    public void setContractEntity1(String contractEntity1) { this.contractEntity1 = contractEntity1; }






    public String getContractBranch1() { return this.contractBranch1; }







    public void setContractBranch1(String contractBranch1) { this.contractBranch1 = contractBranch1; }






    public String getContractContrapart1() { return this.contractContrapart1; }







    public void setContractContrapart1(String contractContrapart1) { this.contractContrapart1 = contractContrapart1; }






    public String getContractfolio1() { return this.contractfolio1; }







    public void setContractfolio1(String contractfolio1) { this.contractfolio1 = contractfolio1; }






    public String getSequenceId1() { return this.sequenceId1; }







    public void setSequenceId1(String sequenceId1) { this.sequenceId1 = sequenceId1; }






    public String getCurrencyOperation() { return this.currencyOperation; }







    public void setCurrencyOperation(String currencyOperation) { this.currencyOperation = currencyOperation; }






    public String getAppReferenceType() { return this.appReferenceType; }







    public void setAppReferenceType(String appReferenceType) { this.appReferenceType = appReferenceType; }






    public String getAppReferenceDesc() { return this.appReferenceDesc; }







    public void setAppReferenceDesc(String appReferenceDesc) { this.appReferenceDesc = appReferenceDesc; }






    public String getAppReferenceCode() { return this.appReferenceCode; }







    public void setAppReferenceCode(String appReferenceCode) { this.appReferenceCode = appReferenceCode; }






    public Integer getDetailRemittancesNumber() { return this.detailRemittancesNumber; }







    public void setDetailRemittancesNumber(Integer detailRemittancesNumber) { this.detailRemittancesNumber = detailRemittancesNumber; }






    public String getOperationType1() { return this.operationType1; }







    public void setOperationType1(String operationType1) { this.operationType1 = operationType1; }






    public String getProcessDate() { return this.processDate; }







    public void setProcessDate(String processDate) { this.processDate = processDate; }






    public String getTakerEntity() { return this.takerEntity; }







    public void setTakerEntity(String takerEntity) { this.takerEntity = takerEntity; }






    public String getTakerBranch() { return this.takerBranch; }







    public void setTakerBranch(String takerBranch) { this.takerBranch = takerBranch; }






    public String getUniqueContractCode1() { return this.uniqueContractCode1; }







    public void setUniqueContractCode1(String uniqueContractCode1) { this.uniqueContractCode1 = uniqueContractCode1; }






    public String getContractEntity2() { return this.contractEntity2; }







    public void setContractEntity2(String contractEntity2) { this.contractEntity2 = contractEntity2; }






    public String getContractBranch2() { return this.contractBranch2; }







    public void setContractBranch2(String contractBranch2) { this.contractBranch2 = contractBranch2; }






    public String getContractContrapart2() { return this.contractContrapart2; }







    public void setContractContrapart2(String contractContrapart2) { this.contractContrapart2 = contractContrapart2; }






    public String getContractfolio2() { return this.contractfolio2; }







    public void setContractfolio2(String contractfolio2) { this.contractfolio2 = contractfolio2; }






    public String getSequenceId2() { return this.sequenceId2; }







    public void setSequenceId2(String sequenceId2) { this.sequenceId2 = sequenceId2; }






    public String getUniqueContractCode2() { return this.uniqueContractCode2; }







    public void setUniqueContractCode2(String uniqueContractCode2) { this.uniqueContractCode2 = uniqueContractCode2; }






    public String getOperationType2() { return this.operationType2; }







    public void setOperationType2(String operationType2) { this.operationType2 = operationType2; }






    public String getCommerceCode() { return this.commerceCode; }







    public void setCommerceCode(String commerceCode) { this.commerceCode = commerceCode; }






    public String getContractType() { return this.contractType; }







    public void setContractType(String contractType) { this.contractType = contractType; }






    public String getCard() { return this.card; }







    public void setCard(String card) { this.card = card; }






    public String getContractCardCode() { return this.contractCardCode; }







    public void setContractCardCode(String contractCardCode) { this.contractCardCode = contractCardCode; }






    public String getContractDetail1() { return this.contractDetail1; }







    public void setContractDetail1(String contractDetail1) { this.contractDetail1 = contractDetail1; }






    public String getContractDetail2() { return this.contractDetail2; }







    public void setContractDetail2(String contractDetail2) { this.contractDetail2 = contractDetail2; }






    public String getClientCode() { return this.clientCode; }







    public void setClientCode(String clientCode) { this.clientCode = clientCode; }






    public String getClientIdentifying() { return this.clientIdentifying; }







    public void setClientIdentifying(String clientIdentifying) { this.clientIdentifying = clientIdentifying; }



    public String getXtiClientIdentifying() { return this.xtiClientIdentifying; }



    public void setXtiClientIdentifying(String xtiClientIdentifying) { this.xtiClientIdentifying = xtiClientIdentifying; }








    public String getCountryCode() { return this.countryCode; }









    public void setCountryCode(String countryCode) { this.countryCode = countryCode; }








    public String getChannelCode() { return this.channelCode; }









    public void setChannelCode(String channelCode) { this.channelCode = channelCode; }








    public String getSubChanelCode() { return this.subChanelCode; }









    public void setSubChanelCode(String subChanelCode) { this.subChanelCode = subChanelCode; }








    public String getErrorCode() { return this.errorCode; }









    public void setErrorCode(String errorCode) { this.errorCode = errorCode; }








    public String getSeverityError() { return this.severityError; }









    public void setSeverityError(String severityError) { this.severityError = severityError; }








    public String getServiceCode() { return this.serviceCode; }









    public void setServiceCode(String serviceCode) { this.serviceCode = serviceCode; }








    public Date getChannelDate() { return this.channelDate; }









    public void setChannelDate(Date channelDate) { this.channelDate = channelDate; }








    public Timestamp getChannelTime() { return this.channelTime; }









    public void setChannelTime(Timestamp channelTime) { this.channelTime = channelTime; }







    public BigDecimal getComisrui() { return this.comisrui; }








    public void setComisrui(BigDecimal comisrui) { this.comisrui = comisrui; }







    public String getChannelAp() { return this.channelAp; }








    public void setChannelAp(String channelAp) { this.channelAp = channelAp; }







    public String getEnvironmentAp() { return this.environmentAp; }








    public void setEnvironmentAp(String environmentAp) { this.environmentAp = environmentAp; }







    public String getServiceAp() { return this.serviceAp; }








    public void setServiceAp(String serviceAp) { this.serviceAp = serviceAp; }








    public String toString() { return "RopInfo [ibanOrigin=" + this.ibanOrigin + ", origAccountOperation=" + this.origAccountOperation + ", origCountryAccount=" + this.origCountryAccount + ", ibanDestiny=" + this.ibanDestiny + ", destAccountOperation=" + this.destAccountOperation + ", destCountryAccount=" + this.destCountryAccount + ", customerType=" + this.customerType + ", customerDocument=" + this.customerDocument + ", relatedUid=" + this.relatedUid + ", currencyCode=" + this.currencyCode + ", amount=" + this.amount + ", referenceType1=" + this.referenceType1 + ", reference1=" + this.reference1 + ", referenceType2=" + this.referenceType2 + ", reference2=" + this.reference2 + ", operationDate=" + this.operationDate + ", totalCode1=" + this.totalCode1 + ", currency1=" + this.currency1 + ", indDeHa1=" + this.indDeHa1 + ", totalAmmount1=" + this.totalAmmount1 + ", totalCode2=" + this.totalCode2 + ", currency2=" + this.currency2 + ", indDeHa2=" + this.indDeHa2 + ", totalAmmount2=" + this.totalAmmount2 + ", rmsCode=" + this.rmsCode + ", currencyOriginalCode=" + this.currencyOriginalCode + ", totalAmmountRms=" + this.totalAmmountRms + ", interestRmsCode=" + this.interestRmsCode + ", totalInterestRms=" + this.totalInterestRms + ", commisionRmsCode=" + this.commisionRmsCode + ", totalCommisionRms=" + this.totalCommisionRms + ", expensesRmsCode=" + this.expensesRmsCode + ", totalExpensesRms=" + this.totalExpensesRms + ", programCode=" + this.programCode + ", customerCode=" + this.customerCode + ", additionalInfo=" + this.additionalInfo + ", totalCode3=" + this.totalCode3 + ", currency3=" + this.currency3 + ", indDeHa3=" + this.indDeHa3 + ", totalAmmount3=" + this.totalAmmount3 + ", totalCode4=" + this.totalCode4 + ", currency4=" + this.currency4 + ", indDeHa4=" + this.indDeHa4 + ", totalAmmount4=" + this.totalAmmount4 + ", numLoteDetails=" + this.numLoteDetails + ", signatureInd=" + this.signatureInd + ", appCode=" + this.appCode + ", operationCode=" + this.operationCode + ", docCode=" + this.docCode + ", amountMIS=" + this.amountMIS + ", contractEntity1=" + this.contractEntity1 + ", contractBranch1=" + this.contractBranch1 + ", contractContrapart1=" + this.contractContrapart1 + ", contractfolio1=" + this.contractfolio1 + ", sequenceId1=" + this.sequenceId1 + ", currencyOperation=" + this.currencyOperation + ", appReferenceType=" + this.appReferenceType + ", appReferenceDesc=" + this.appReferenceDesc + ", appReferenceCode=" + this.appReferenceCode + ", detailRemittancesNumber=" + this.detailRemittancesNumber + ", operationType1=" + this.operationType1 + ", processDate=" + this.processDate + ", takerEntity=" + this.takerEntity + ", takerBranch=" + this.takerBranch + ", uniqueContractCode1=" + this.uniqueContractCode1 + ", contractEntity2=" + this.contractEntity2 + ", contractBranch2=" + this.contractBranch2 + ", contractContrapart2=" + this.contractContrapart2 + ", contractfolio2=" + this.contractfolio2 + ", sequenceId2=" + this.sequenceId2 + ", uniqueContractCode2=" + this.uniqueContractCode2 + ", operationType2=" + this.operationType2 + ", commerceCode=" + this.commerceCode + ", contractType=" + this.contractType + ", card=" + this.card + ", contractCardCode=" + this.contractCardCode + ", contractDetail1=" + this.contractDetail1 + ", contractDetail2=" + this.contractDetail2 + ", clientCode=" + this.clientCode + ", clientIdentifying=" + this.clientIdentifying + ", xtiClientIdentifying=" + this.xtiClientIdentifying + ", countryCode = " + this.countryCode + ", channelCode = " + this.channelCode + ", subChanelCode = " + this.subChanelCode + ", errorCode = " + this.errorCode + ", severityError = " + this.severityError + ", serviceCode = " + this.serviceCode + ", channelDate = " + this.channelDate + ", channelTime = " + this.channelTime + ", comisrui = " + this.comisrui + ", channelAp = " + this.channelAp + ", environmentAp = " + this.environmentAp + ", serviceAp = " + this.serviceAp + "]"; }
































































    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        RopInfo other = (RopInfo)obj;
        if (this.additionalInfo == null) {
            if (other.additionalInfo != null)
                return false;
        } else if (!this.additionalInfo.equals(other.additionalInfo)) {
            return false;
        }  if (this.amount == null) {
            if (other.amount != null)
                return false;
        } else if (!this.amount.equals(other.amount)) {
            return false;
        }  if (this.amountMIS == null) {
            if (other.amountMIS != null)
                return false;
        } else if (!this.amountMIS.equals(other.amountMIS)) {
            return false;
        }  if (this.appCode == null) {
            if (other.appCode != null)
                return false;
        } else if (!this.appCode.equals(other.appCode)) {
            return false;
        }  if (this.appReferenceCode == null) {
            if (other.appReferenceCode != null)
                return false;
        } else if (!this.appReferenceCode.equals(other.appReferenceCode)) {
            return false;
        }  if (this.appReferenceDesc == null) {
            if (other.appReferenceDesc != null)
                return false;
        } else if (!this.appReferenceDesc.equals(other.appReferenceDesc)) {
            return false;
        }  if (this.appReferenceType == null) {
            if (other.appReferenceType != null)
                return false;
        } else if (!this.appReferenceType.equals(other.appReferenceType)) {
            return false;
        }  if (this.card == null) {
            if (other.card != null)
                return false;
        } else if (!this.card.equals(other.card)) {
            return false;
        }  if (this.clientCode == null) {
            if (other.clientCode != null)
                return false;
        } else if (!this.clientCode.equals(other.clientCode)) {
            return false;
        }  if (this.clientIdentifying == null) {
            if (other.clientIdentifying != null)
                return false;
        } else if (!this.clientIdentifying.equals(other.clientIdentifying)) {
            return false;
        }  if (this.commerceCode == null) {
            if (other.commerceCode != null)
                return false;
        } else if (!this.commerceCode.equals(other.commerceCode)) {
            return false;
        }  if (this.commisionRmsCode == null) {
            if (other.commisionRmsCode != null)
                return false;
        } else if (!this.commisionRmsCode.equals(other.commisionRmsCode)) {
            return false;
        }  if (this.contractBranch1 == null) {
            if (other.contractBranch1 != null)
                return false;
        } else if (!this.contractBranch1.equals(other.contractBranch1)) {
            return false;
        }  if (this.contractBranch2 == null) {
            if (other.contractBranch2 != null)
                return false;
        } else if (!this.contractBranch2.equals(other.contractBranch2)) {
            return false;
        }  if (this.contractCardCode == null) {
            if (other.contractCardCode != null)
                return false;
        } else if (!this.contractCardCode.equals(other.contractCardCode)) {
            return false;
        }  if (this.contractContrapart1 == null) {
            if (other.contractContrapart1 != null)
                return false;
        } else if (!this.contractContrapart1.equals(other.contractContrapart1)) {
            return false;
        }  if (this.contractContrapart2 == null) {
            if (other.contractContrapart2 != null)
                return false;
        } else if (!this.contractContrapart2.equals(other.contractContrapart2)) {
            return false;
        }  if (this.contractDetail1 == null) {
            if (other.contractDetail1 != null)
                return false;
        } else if (!this.contractDetail1.equals(other.contractDetail1)) {
            return false;
        }  if (this.contractDetail2 == null) {
            if (other.contractDetail2 != null)
                return false;
        } else if (!this.contractDetail2.equals(other.contractDetail2)) {
            return false;
        }  if (this.contractEntity1 == null) {
            if (other.contractEntity1 != null)
                return false;
        } else if (!this.contractEntity1.equals(other.contractEntity1)) {
            return false;
        }  if (this.contractEntity2 == null) {
            if (other.contractEntity2 != null)
                return false;
        } else if (!this.contractEntity2.equals(other.contractEntity2)) {
            return false;
        }  if (this.contractType == null) {
            if (other.contractType != null)
                return false;
        } else if (!this.contractType.equals(other.contractType)) {
            return false;
        }  if (this.contractfolio1 == null) {
            if (other.contractfolio1 != null)
                return false;
        } else if (!this.contractfolio1.equals(other.contractfolio1)) {
            return false;
        }  if (this.contractfolio2 == null) {
            if (other.contractfolio2 != null)
                return false;
        } else if (!this.contractfolio2.equals(other.contractfolio2)) {
            return false;
        }  if (this.currency1 == null) {
            if (other.currency1 != null)
                return false;
        } else if (!this.currency1.equals(other.currency1)) {
            return false;
        }  if (this.currency2 == null) {
            if (other.currency2 != null)
                return false;
        } else if (!this.currency2.equals(other.currency2)) {
            return false;
        }  if (this.currency3 == null) {
            if (other.currency3 != null)
                return false;
        } else if (!this.currency3.equals(other.currency3)) {
            return false;
        }  if (this.currency4 == null) {
            if (other.currency4 != null)
                return false;
        } else if (!this.currency4.equals(other.currency4)) {
            return false;
        }  if (this.currencyCode == null) {
            if (other.currencyCode != null)
                return false;
        } else if (!this.currencyCode.equals(other.currencyCode)) {
            return false;
        }  if (this.currencyOperation == null) {
            if (other.currencyOperation != null)
                return false;
        } else if (!this.currencyOperation.equals(other.currencyOperation)) {
            return false;
        }  if (this.currencyOriginalCode == null) {
            if (other.currencyOriginalCode != null)
                return false;
        } else if (!this.currencyOriginalCode.equals(other.currencyOriginalCode)) {
            return false;
        }  if (this.customerCode == null) {
            if (other.customerCode != null)
                return false;
        } else if (!this.customerCode.equals(other.customerCode)) {
            return false;
        }  if (this.customerDocument == null) {
            if (other.customerDocument != null)
                return false;
        } else if (!this.customerDocument.equals(other.customerDocument)) {
            return false;
        }  if (this.customerType == null) {
            if (other.customerType != null)
                return false;
        } else if (!this.customerType.equals(other.customerType)) {
            return false;
        }  if (this.destAccountOperation == null) {
            if (other.destAccountOperation != null)
                return false;
        } else if (!this.destAccountOperation.equals(other.destAccountOperation)) {
            return false;
        }  if (this.destCountryAccount == null) {
            if (other.destCountryAccount != null)
                return false;
        } else if (!this.destCountryAccount.equals(other.destCountryAccount)) {
            return false;
        }  if (this.detailRemittancesNumber == null) {
            if (other.detailRemittancesNumber != null) {
                return false;
            }
        } else if (!this.detailRemittancesNumber.equals(other.detailRemittancesNumber)) {
            return false;
        }  if (this.docCode == null) {
            if (other.docCode != null)
                return false;
        } else if (!this.docCode.equals(other.docCode)) {
            return false;
        }  if (this.expensesRmsCode == null) {
            if (other.expensesRmsCode != null)
                return false;
        } else if (!this.expensesRmsCode.equals(other.expensesRmsCode)) {
            return false;
        }  if (this.ibanDestiny == null) {
            if (other.ibanDestiny != null)
                return false;
        } else if (!this.ibanDestiny.equals(other.ibanDestiny)) {
            return false;
        }  if (this.ibanOrigin == null) {
            if (other.ibanOrigin != null)
                return false;
        } else if (!this.ibanOrigin.equals(other.ibanOrigin)) {
            return false;
        }  if (this.indDeHa1 == null) {
            if (other.indDeHa1 != null)
                return false;
        } else if (!this.indDeHa1.equals(other.indDeHa1)) {
            return false;
        }  if (this.indDeHa2 == null) {
            if (other.indDeHa2 != null)
                return false;
        } else if (!this.indDeHa2.equals(other.indDeHa2)) {
            return false;
        }  if (this.indDeHa3 == null) {
            if (other.indDeHa3 != null)
                return false;
        } else if (!this.indDeHa3.equals(other.indDeHa3)) {
            return false;
        }  if (this.indDeHa4 == null) {
            if (other.indDeHa4 != null)
                return false;
        } else if (!this.indDeHa4.equals(other.indDeHa4)) {
            return false;
        }  if (this.interestRmsCode == null) {
            if (other.interestRmsCode != null)
                return false;
        } else if (!this.interestRmsCode.equals(other.interestRmsCode)) {
            return false;
        }  if (this.numLoteDetails == null) {
            if (other.numLoteDetails != null)
                return false;
        } else if (!this.numLoteDetails.equals(other.numLoteDetails)) {
            return false;
        }  if (this.operationCode == null) {
            if (other.operationCode != null)
                return false;
        } else if (!this.operationCode.equals(other.operationCode)) {
            return false;
        }  if (this.operationDate == null) {
            if (other.operationDate != null)
                return false;
        } else if (!this.operationDate.equals(other.operationDate)) {
            return false;
        }  if (this.operationType1 == null) {
            if (other.operationType1 != null)
                return false;
        } else if (!this.operationType1.equals(other.operationType1)) {
            return false;
        }  if (this.operationType2 == null) {
            if (other.operationType2 != null)
                return false;
        } else if (!this.operationType2.equals(other.operationType2)) {
            return false;
        }  if (this.origAccountOperation == null) {
            if (other.origAccountOperation != null)
                return false;
        } else if (!this.origAccountOperation.equals(other.origAccountOperation)) {
            return false;
        }  if (this.origCountryAccount == null) {
            if (other.origCountryAccount != null)
                return false;
        } else if (!this.origCountryAccount.equals(other.origCountryAccount)) {
            return false;
        }  if (this.processDate == null) {
            if (other.processDate != null)
                return false;
        } else if (!this.processDate.equals(other.processDate)) {
            return false;
        }  if (this.programCode == null) {
            if (other.programCode != null)
                return false;
        } else if (!this.programCode.equals(other.programCode)) {
            return false;
        }  if (this.reference1 == null) {
            if (other.reference1 != null)
                return false;
        } else if (!this.reference1.equals(other.reference1)) {
            return false;
        }  if (this.reference2 == null) {
            if (other.reference2 != null)
                return false;
        } else if (!this.reference2.equals(other.reference2)) {
            return false;
        }  if (this.referenceType1 == null) {
            if (other.referenceType1 != null)
                return false;
        } else if (!this.referenceType1.equals(other.referenceType1)) {
            return false;
        }  if (this.referenceType2 == null) {
            if (other.referenceType2 != null)
                return false;
        } else if (!this.referenceType2.equals(other.referenceType2)) {
            return false;
        }  if (this.relatedUid == null) {
            if (other.relatedUid != null)
                return false;
        } else if (!this.relatedUid.equals(other.relatedUid)) {
            return false;
        }  if (this.rmsCode == null) {
            if (other.rmsCode != null)
                return false;
        } else if (!this.rmsCode.equals(other.rmsCode)) {
            return false;
        }  if (this.sequenceId1 == null) {
            if (other.sequenceId1 != null)
                return false;
        } else if (!this.sequenceId1.equals(other.sequenceId1)) {
            return false;
        }  if (this.sequenceId2 == null) {
            if (other.sequenceId2 != null)
                return false;
        } else if (!this.sequenceId2.equals(other.sequenceId2)) {
            return false;
        }  if (this.signatureInd == null) {
            if (other.signatureInd != null)
                return false;
        } else if (!this.signatureInd.equals(other.signatureInd)) {
            return false;
        }  if (this.takerBranch == null) {
            if (other.takerBranch != null)
                return false;
        } else if (!this.takerBranch.equals(other.takerBranch)) {
            return false;
        }  if (this.takerEntity == null) {
            if (other.takerEntity != null)
                return false;
        } else if (!this.takerEntity.equals(other.takerEntity)) {
            return false;
        }  if (this.totalAmmount1 == null) {
            if (other.totalAmmount1 != null)
                return false;
        } else if (!this.totalAmmount1.equals(other.totalAmmount1)) {
            return false;
        }  if (this.totalAmmount2 == null) {
            if (other.totalAmmount2 != null)
                return false;
        } else if (!this.totalAmmount2.equals(other.totalAmmount2)) {
            return false;
        }  if (this.totalAmmount3 == null) {
            if (other.totalAmmount3 != null)
                return false;
        } else if (!this.totalAmmount3.equals(other.totalAmmount3)) {
            return false;
        }  if (this.totalAmmount4 == null) {
            if (other.totalAmmount4 != null)
                return false;
        } else if (!this.totalAmmount4.equals(other.totalAmmount4)) {
            return false;
        }  if (this.totalAmmountRms == null) {
            if (other.totalAmmountRms != null)
                return false;
        } else if (!this.totalAmmountRms.equals(other.totalAmmountRms)) {
            return false;
        }  if (this.totalCode1 == null) {
            if (other.totalCode1 != null)
                return false;
        } else if (!this.totalCode1.equals(other.totalCode1)) {
            return false;
        }  if (this.totalCode2 == null) {
            if (other.totalCode2 != null)
                return false;
        } else if (!this.totalCode2.equals(other.totalCode2)) {
            return false;
        }  if (this.totalCode3 == null) {
            if (other.totalCode3 != null)
                return false;
        } else if (!this.totalCode3.equals(other.totalCode3)) {
            return false;
        }  if (this.totalCode4 == null) {
            if (other.totalCode4 != null)
                return false;
        } else if (!this.totalCode4.equals(other.totalCode4)) {
            return false;
        }  if (this.totalCommisionRms == null) {
            if (other.totalCommisionRms != null)
                return false;
        } else if (!this.totalCommisionRms.equals(other.totalCommisionRms)) {
            return false;
        }  if (this.totalExpensesRms == null) {
            if (other.totalExpensesRms != null)
                return false;
        } else if (!this.totalExpensesRms.equals(other.totalExpensesRms)) {
            return false;
        }  if (this.totalInterestRms == null) {
            if (other.totalInterestRms != null)
                return false;
        } else if (!this.totalInterestRms.equals(other.totalInterestRms)) {
            return false;
        }  if (this.uniqueContractCode1 == null) {
            if (other.uniqueContractCode1 != null)
                return false;
        } else if (!this.uniqueContractCode1.equals(other.uniqueContractCode1)) {
            return false;
        }  if (this.uniqueContractCode2 == null) {
            if (other.uniqueContractCode2 != null)
                return false;
        } else if (!this.uniqueContractCode2.equals(other.uniqueContractCode2)) {
            return false;
        }  if (this.xtiClientIdentifying == null) {
            if (other.xtiClientIdentifying != null)
                return false;
        } else if (!this.xtiClientIdentifying.equals(other.xtiClientIdentifying)) {
            return false;
        }  if (this.countryCode == null) {
            if (other.countryCode != null)
                return false;
        } else if (!this.countryCode.equals(other.countryCode)) {
            return false;
        }  if (this.channelCode == null) {
            if (other.channelCode != null)
                return false;
        } else if (!this.channelCode.equals(other.channelCode)) {
            return false;
        }  if (this.subChanelCode == null) {
            if (other.subChanelCode != null)
                return false;
        } else if (!this.subChanelCode.equals(other.subChanelCode)) {
            return false;
        }  if (this.errorCode == null) {
            if (other.errorCode != null)
                return false;
        } else if (!this.errorCode.equals(other.errorCode)) {
            return false;
        }  if (this.severityError == null) {
            if (other.severityError != null)
                return false;
        } else if (!this.severityError.equals(other.severityError)) {
            return false;
        }  if (this.serviceCode == null) {
            if (other.serviceCode != null)
                return false;
        } else if (!this.serviceCode.equals(other.serviceCode)) {
            return false;
        }  if (this.channelDate == null) {
            if (other.channelDate != null)
                return false;
        } else if (!this.channelDate.equals(other.channelDate)) {
            return false;
        }  if (this.channelTime == null) {
            if (other.channelTime != null)
                return false;
        } else if (!this.channelTime.equals(other.channelTime)) {
            return false;
        }  if (this.comisrui == null) {
            if (other.comisrui != null)
                return false;
        } else if (!this.comisrui.equals(other.comisrui)) {
            return false;
        }  if (this.channelAp == null) {
            if (other.channelAp != null)
                return false;
        } else if (!this.channelAp.equals(other.channelAp)) {
            return false;
        }  if (this.environmentAp == null) {
            if (other.environmentAp != null)
                return false;
        } else if (!this.environmentAp.equals(other.environmentAp)) {
            return false;
        }  if (this.serviceAp == null) {
            if (other.serviceAp != null)
                return false;
        } else if (!this.serviceAp.equals(other.serviceAp)) {
            return false;
        }  return true;
    }
}
