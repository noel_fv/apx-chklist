package com.example.demo.pcld.utils.elara;

import java.io.Serializable;

public enum PhysicalProtocol implements Serializable {
    JMS,


    REST,


    WS;
}