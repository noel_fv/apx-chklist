package com.example.demo.pcld.utils.elara;

//import com.bbva.elara.domain.transaction.ThreadContext;
//import com.bbva.elara.support.jdbc.manager.JdbcManager;
//import com.bbva.elara.utility.jdbc.connector.JdbcUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.commons.io.FileUtils;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
D:\ENTORNO_LOCAL_APX\dirBaseRepositorio\qwpo\online\jar\modules\arq\core
elara-utility-jdbc.jar
elara-utility-jdbc-connector.jar
elara-utility-jdbc-connector-impl.jar
*/
public class JdbcUtilsImpl
  implements JdbcUtils, UtilsControl
{
  private static final Logger LOGGER = LoggerFactory.getLogger(JdbcUtilsImpl.class);
  private static final String DEFAULT_CHARSET = "UTF-8";
  private int maxRows;
  private String warningCodeMoreRows;
  private JdbcManager jdbcManager;
  private String uuaa;
  private BundleContext bundleContext;
  private Map<String, String> sqls;
  public JdbcUtilsImpl() {}

  public JdbcUtilsImpl(BundleContext bundleContext, String warningCodeMoreRows, int maxRows, JdbcManager jdbcManager) throws IOException {
    this.sqls = new ConcurrentHashMap();
    this.maxRows = maxRows;
    this.warningCodeMoreRows = warningCodeMoreRows;
    this.bundleContext = bundleContext;
    this.jdbcManager = jdbcManager;
    this.uuaa = bundleContext.getBundle().getSymbolicName().substring(0, 4).toLowerCase();
    getSqFromFile();
    LOGGER.info("JdbcConnector created for uuaa: " + this.uuaa);
  }
  
  public int[] batchUpdate(String queryCode, List<Object[]> batchArgs) {
    LOGGER.info("batch Update with query code: " + queryCode);
    String sql = parseQuery(queryCode);
    return getCommonJdbcTemplateFromQueryCode(queryCode).batchUpdate(sql, batchArgs);
  }

  
  public int[] batchUpdate(String queryCode, List<Object[]> batchArgs, int[] argTypes) {
    LOGGER.info("batch Update with query code: " + queryCode);
    String sql = parseQuery(queryCode);
    return getCommonJdbcTemplateFromQueryCode(queryCode).batchUpdate(sql, batchArgs, argTypes);
  }


  public int queryForInt(String queryCode, Map<String, ?> args) {
    LOGGER.info("Query for int with query code: " + queryCode);
    String sql = parseQuery(queryCode, args);
    return getCommonJdbcTemplateFromQueryCode(queryCode).queryForInt(sql, args);
  }

  
  public int queryForInt(String queryCode, Object... args) {
    LOGGER.info("Query for int with query code: " + queryCode);
    String sql = parseQuery(queryCode, args);
    CommonJdbcTemplate template = getCommonJdbcTemplateFromQueryCode(queryCode);
    Integer res = Integer.valueOf(template.queryForInt(sql, args));
    return res.intValue();
  }

  
  public List<Map<String, Object>> queryForList(String queryCode, Object... args) {
    LOGGER.info("Query for list with query code: " + queryCode);
    String sql = parseQuery(queryCode, args);
    
    List<Map<String, Object>> result = getCommonJdbcTemplateFromQueryCode(queryCode).queryForList(sql, args);
    
    if (checkIfMoreElements(result, queryCode)) {
      result = result.subList(0, this.maxRows);
    }
    return result;
  }

  public List<Map<String, Object>> pagingQueryForList(String queryCode, int firstRow, int pageSize, Object... args) {
    LOGGER.info("Paging query for list with query code: " + queryCode + " ,first row: " + firstRow + " and page size: " + pageSize);

    
    String sql = parseQueryWithPagination(queryCode, firstRow, pageSize, args);
    List<Map<String, Object>> result = getCommonJdbcTemplateFromQueryCode(queryCode).queryForList(sql, args);

    
    if (checkIfMoreElements(result, queryCode)) {
      result = result.subList(0, pageSize);
    }
    
    return result;
  }






  
  public List<Map<String, Object>> queryForList(String queryCode) {
    LOGGER.info("Query for list with query code: " + queryCode);
    String sql = parseQuery(queryCode);
    List<Map<String, Object>> result = getCommonJdbcTemplateFromQueryCode(queryCode).queryForList(sql, new Object[0]);
    if (checkIfMoreElements(result, queryCode)) {
      result = result.subList(0, this.maxRows);
    }
    return result;
  }

  
  public List<Map<String, Object>> pagingQueryForList(String queryCode, int firstRow, int pageSize) {
    LOGGER.info("Paging query for list with query code: " + queryCode + " ,first row: " + firstRow + " and page size: " + pageSize);
    
    String sql = parseQueryWithPagination(queryCode, firstRow, pageSize);
    List<Map<String, Object>> result = getCommonJdbcTemplateFromQueryCode(queryCode).queryForList(sql, new Object[0]);
    if (checkIfMoreElements(result, queryCode)) {
      result = result.subList(0, pageSize);
    }
    return result;
  }

  public List<Map<String, Object>> queryForList(String queryCode, Map<String, ?> args) {
    LOGGER.info("Query for list with query code: " + queryCode);
    String sql = parseQuery(queryCode, args);
    List<Map<String, Object>> result = getCommonJdbcTemplateFromQueryCode(queryCode).queryForList(sql, args);
    
    if (checkIfMoreElements(result, queryCode)) {
      result = result.subList(0, this.maxRows);
    }
    return result;
  }
  
  public List<Map<String, Object>> pagingQueryForList(String queryCode, int firstRow, int pageSize, Map<String, ?> args) {
    LOGGER.info("Paging query for list with query code: " + queryCode + " ,first row: " + firstRow + " and page size: " + pageSize);
    
    String sql = parseQueryWithPagination(queryCode, firstRow, pageSize, args);
    List<Map<String, Object>> result = getCommonJdbcTemplateFromQueryCode(queryCode).queryForList(sql, args);
    
    if (checkIfMoreElements(result, queryCode)) {
      result = result.subList(0, pageSize);
    }
    return result;
  }
  
  public ParameterTable queryForTable(String queryCode, Map<String, ?> args) {
    LOGGER.info("Query for table with query code: " + queryCode);
    String sql = parseQuery(queryCode, args);
    
    List<Map<String, Object>> list = getCommonJdbcTemplateFromQueryCode(queryCode).queryForList(sql, args);
    
    return assingDataTable(list, queryCode, this.maxRows);
  }

  public ParameterTable pagingQueryForTable(String queryCode, int firstRow, int pageSize, Map<String, ?> args) {
    LOGGER.info("Paging query for table with query code: " + queryCode + " ,first row: " + firstRow + " and page size: " + pageSize);
    
    String sql = parseQueryWithPagination(queryCode, firstRow, pageSize, args);

    
    List<Map<String, Object>> list = getCommonJdbcTemplateFromQueryCode(queryCode).queryForList(sql, args);
    
    return assingDataTable(list, queryCode, pageSize);
  }

  
  public ParameterTable queryForTable(String queryCode, Object... args) {
    LOGGER.info("Query for table with query code: " + queryCode);
    String sql = parseQuery(queryCode, args);
    List<Map<String, Object>> list = getCommonJdbcTemplateFromQueryCode(queryCode).queryForList(sql, args);
    
    return assingDataTable(list, queryCode, this.maxRows);
  }

  
  public ParameterTable pagingQueryForTable(String queryCode, int firstRow, int pageSize, Object... args) {
    LOGGER.info("Paging query for table with query code: " + queryCode + " ,first row: " + firstRow + " and page size: " + pageSize);
    
    String sql = parseQueryWithPagination(queryCode, firstRow, pageSize, args);
    List<Map<String, Object>> list = getCommonJdbcTemplateFromQueryCode(queryCode).queryForList(sql, args);
    
    return assingDataTable(list, queryCode, pageSize);
  }

  
  public String queryForString(String queryCode, Map<String, ?> args) {
    LOGGER.info("Query for string with query code: " + queryCode);
    String sql = parseQuery(queryCode, args);
    return (String)getCommonJdbcTemplateFromQueryCode(queryCode)
      .queryForObject(sql, String.class, args);
  }

  
  public String queryForString(String queryCode, Object... args) {
    LOGGER.info("Query for string with query code: " + queryCode);
    String sql = parseQuery(queryCode, args);
    return (String)getCommonJdbcTemplateFromQueryCode(queryCode)
      .queryForObject(sql, String.class, args);
  }


  public long queryForLong(String queryCode, Map<String, ?> args) {
    LOGGER.info("Query for long with query code: " + queryCode);
    String sql = parseQuery(queryCode, args);
    return getCommonJdbcTemplateFromQueryCode(queryCode).queryForLong(sql, args);
  }


  public long queryForLong(String queryCode, Object... args) {
    LOGGER.info("Query for long with query code: " + queryCode);
    String sql = parseQuery(queryCode, args);
    return getCommonJdbcTemplateFromQueryCode(queryCode).queryForLong(sql, args);
  }

  
  public Map<String, Object> queryForMap(String queryCode, Map<String, ?> args) {
    LOGGER.info("Query for map with query code: " + queryCode);
    String sql = parseQuery(queryCode, args);
    return getCommonJdbcTemplateFromQueryCode(queryCode).queryForMap(sql, args);
  }


  public Map<String, Object> pagingQueryForMap(String queryCode, int firstRow, int pageSize, Map<String, ?> args) {
    LOGGER.info("Paging query for map with query code: " + queryCode + " ,first row: " + firstRow + " and page size: " + pageSize);
    
    String sql = parseQueryWithPagination(queryCode, firstRow, pageSize, args);
    
    return getCommonJdbcTemplateFromQueryCode(queryCode).queryForMap(sql, args);
  }

  
  public Map<String, Object> queryForMap(String queryCode, Object... args) {
    LOGGER.info("Query for map with query code: " + queryCode);
    String sql = parseQuery(queryCode, args);
    return getCommonJdbcTemplateFromQueryCode(queryCode).queryForMap(sql, args);
  }
 
  public Map<String, Object> pagingQueryForMap(String queryCode, int firstRow, int pageSize, Object... args) {
    LOGGER.info("Paging query for map with query code: " + queryCode + " ,first row: " + firstRow + " and page size: " + pageSize);
    
    String sql = parseQueryWithPagination(queryCode, firstRow, pageSize, args);
    
    return getCommonJdbcTemplateFromQueryCode(queryCode).queryForMap(sql, args);
  }



  private boolean checkIfMoreElements(List<?> result, String queryCode) {
    Boolean bRetorno = Boolean.FALSE;
    if (result.size() > this.maxRows) {
      LOGGER.warn(this.warningCodeMoreRows, queryCode);
      bRetorno = Boolean.TRUE;
    } 
    return bRetorno.booleanValue();
  }

  
  public int[] batchUpdate(String queryCode, Map[] batchValues) {
    LOGGER.info("batch Update with query code: " + queryCode);
    String sql = parseQuery(queryCode);
    return getCommonJdbcTemplateFromQueryCode(queryCode).batchUpdate(sql, batchValues);
  }

  
  public int update(String queryCode, Map<String,Object> args) {
    LOGGER.info("Updating DB with query code: " + queryCode);
    String sql = parseQuery(queryCode, args);
    return getCommonJdbcTemplateFromQueryCode(queryCode).update(sql, args);
  }
  
  public int update(String queryCode, Object... args) {
    LOGGER.info("Updating DB with query code: " + queryCode);
    String sql = parseQuery(queryCode, args);
    return getCommonJdbcTemplateFromQueryCode(queryCode).update(sql, args);
  }

  
  private String parseQuery(String code) {
    LOGGER.info("Getting query: " + code);
    String sql = null;
    String queryLine = getSqlLine(code);
    String[] queryLineProperties = queryLine.split(";");
    sql = queryLineProperties[1];
    QueryExecutorLogger.traceQuerySQL(code, sql);
    LOGGER.info("Getted query: " + code + " ,with SQL: " + sql);
    return sql;
  }


  private String parseQueryWithPagination(String code, int firstRow, int pageSize) {
    LOGGER.info("Getting query: " + code);
    String sql = insertPagination(parseQuery(code), firstRow, pageSize);
    
    QueryExecutorLogger.traceQuery(code, new Object[0]);
    LOGGER.info("Getted query: " + code + " ,with SQL: " + sql);
    return sql;
  }

  
  private String parseQuery(String code, Object[] args) {
    LOGGER.info("Getting query: " + code);
    String sql = parseQuery(code);
    QueryExecutorLogger.traceQuery(code, args);
    LOGGER.info("Getted query: " + code + " ,with SQL: " + sql);
    return sql;
  }

  
  private String parseQueryWithPagination(String code, int firstRow, int pageSize, Object[] args) {
    LOGGER.info("Getting query: " + code);
    String sql = insertPagination(parseQuery(code), firstRow, pageSize);
    
    QueryExecutorLogger.traceQuery(code, args);
    LOGGER.info("Getted query: " + code + " ,with SQL: " + sql);
    return sql;
  }

  
  private String parseQuery(String code, Map<String, ?> args) {
    LOGGER.info("Getting query: " + code);
    String sql = parseQuery(code);
    QueryExecutorLogger.traceQuery(code, args);
    LOGGER.info("Getted query: " + code + " ,with SQL: " + sql);
    return sql;
  }

  
  private String parseQueryWithPagination(String code, int firstRow, int pageSize, Map<String, ?> args) {
    LOGGER.info("Getting query: " + code);
    String sql = insertPagination(parseQuery(code), firstRow, pageSize);
    
    QueryExecutorLogger.traceQuery(code, args);
    LOGGER.info("Getted query: " + code + " ,with SQL: " + sql);
    return sql;
  }


  private String insertPagination(String parseQuery, int firstRow, int pageSize) {
    StringBuilder builder = new StringBuilder();
    builder.append(parseQuery);
    builder.append(" OFFSET ").append(firstRow - 1);
    builder.append(" ROWS FETCH NEXT ").append(pageSize)
      .append(" ROWS ONLY");
    return builder.toString();
  }

  
  public int updateLob(String queryCode, Map<String, Object> args) {
    LOGGER.info("Updating DB with query code: " + queryCode);
    String sql = parseQuery(queryCode, args);
    Map<String, Object> argsFinal = new HashMap<String, Object>();
    for (Map.Entry<String, ?> entry : args.entrySet()) {
      if (entry.getValue() instanceof File)
        try {
          if (isAsciiText((File)entry.getValue())) {
            argsFinal.put(entry.getKey(), 
                FileUtils.readFileToString((File)entry.getValue(), "UTF-8"));
            continue;
          } 
          argsFinal.put(entry.getKey(), 
              FileUtils.readFileToByteArray((File)entry.getValue()));
          continue;
        } catch (IOException e) {
          LOGGER.error("Error to convert file");
          continue;
        }  
      argsFinal.put(entry.getKey(), entry.getValue());
    } 
    
    return getCommonJdbcTemplateFromQueryCode(queryCode).update(sql, argsFinal);
  }

  
  public int updateLob(String queryCode, Object... args) {
    LOGGER.info("Updating DB with query code: " + queryCode);
    String sql = parseQuery(queryCode, args);
    List<Object> argsFinal = new ArrayList<Object>();
    for (Object object : args) {
      try {
        if (object instanceof File) {
          if (isAsciiText((File)object)) {
            argsFinal.add(FileUtils.readFileToString((File)object, "UTF-8"));
          } else {
            
            argsFinal.add(
                FileUtils.readFileToByteArray((File)object));
          } 
        } else {
          argsFinal.add(object);
        } 
      } catch (IOException e) {
        LOGGER.error("Error to convert file");
      } 
    } 
    return getCommonJdbcTemplateFromQueryCode(queryCode).update(sql, argsFinal
        .toArray());
  }
  
  private static boolean isAsciiText(File fileName) throws IOException {
    byte[] bytes = new byte[500];
    boolean result = true;
    try (InputStream inp = new FileInputStream(fileName)) {
      inp.read(bytes, 0, bytes.length);
      inp.close();
     int bin = 0;
      for (byte thisByte : bytes) {
        char charValue = (char)thisByte;
        if (!Character.isWhitespace(charValue) && 
          Character.isISOControl(charValue)) {
          bin = (short)(bin + 1);
        }
        if (bin >= 5) {
          result = false;
        }
      } 
    } 
    return result;
  }


  private String parseDB(String queryLine) {
    return queryLine.split(";")[0];
  }


  
  private CommonJdbcTemplate getCommonJdbcTemplateFromQueryCode(String queryCode) {
    String queryLine = getSqlLine(queryCode);
    String dbResource = parseDB(queryLine);
    return this.jdbcManager.getJdbcConnection(this.uuaa, dbResource);
  }

  
  private void getSqFromFile() throws IOException {
    Bundle bundle = this.bundleContext.getBundle();
    String bundleName = (String)bundle.getHeaders().get("Bundle-Name");
    LOGGER.debug("Finding SQL file for bundle name {}", bundleName);
    Enumeration<URL> urlEnumeration = bundle.findEntries("/", "sql-" + bundleName + ".properties", true);
    
    if (urlEnumeration == null) {
      LOGGER.info("The SQL file is missing for bundle name {}", bundleName);
      urlEnumeration = bundle.findEntries("/", "sql-" + bundleName + "*.properties", true);
      
      if (urlEnumeration == null) {
        LOGGER.error("The SQL file is missing for {}", bundleName);
      } else {
        fillSqls(urlEnumeration, bundleName);
      } 
    } else {
      fillSqls(urlEnumeration, bundleName);
    } 
  }

  
  private void fillSqls(Enumeration<URL> urlEnumeration, String bundleName) throws IOException {
    InputStream sqlProperty = null;
    try {
      Properties configFile = new Properties();
      while (urlEnumeration.hasMoreElements()) {
        URL sqlURL = (URL)urlEnumeration.nextElement();
        
        if (sqlURL != null) {
          LOGGER.debug("reading sql File {}", sqlURL);
          if (sqlProperty != null)
             sqlProperty.close();
           sqlProperty = sqlURL.openStream();
        } 
      } 
      if (sqlProperty != null) {
        Reader reader = new InputStreamReader(sqlProperty);
        configFile.load(reader);
        for (Map.Entry<Object, Object> entry : configFile.entrySet()) {
          this.sqls.put((String)entry.getKey(), (String)entry
              .getValue());
        }
      } else {
        LOGGER.error("The SQL file is missing for bundle {}", bundleName);
      } 
    } finally {
      
      if (sqlProperty != null)
        sqlProperty.close(); 
    } 
  }
  
  private String getSqlLine(String queryCode) {
    String sql = (String)ThreadContext.get().getDynamicSqls().get(queryCode);
    
    if (sql == null) {
      return (String)this.sqls.get(queryCode);
    }
    return sql;
  }


  private ParameterTable assingDataTable(List<Map<String, Object>> list, String queryCode, int rowSize) {
    ParameterTable table = new ParameterTable();
    if (!list.isEmpty()) {
      if (checkIfMoreElements(list, queryCode)) {
        list = list.subList(0, rowSize);
      }
      table.setParameterTableList(list);
    } 
    return table;
  }


  public String getQuery(String code) { 
  return parseQuery(code);
  }

  
  public void addQuery(String code, String sqlLine) {
    LOGGER.info("Adding query: " + code + " ,with resource+SQL: " + sqlLine);
    ThreadContext.get().addDynamicSql(code, sqlLine);
    LOGGER.info("Added query: " + code + " ,with resource+SQL: " + sqlLine);
  }

  public String getUuaa() { return this.uuaa; }
}
