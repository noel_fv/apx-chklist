package com.example.demo.pcld.utils.elara;

//import com.bbva.elara.domain.integration.enumerated.LogicalProtocol;
//import com.bbva.elara.domain.integration.enumerated.PhysicalProtocol;

import java.io.Serializable;

import org.apache.cxf.jaxrs.ext.MessageContext;


public class TransactionRequest
        implements Serializable {
    private static final long serialVersionUID = 1L;
    private String messageID;
    private String uid;
    private LogicalProtocol logicalProtocol;
    private PhysicalProtocol physicalProtocol;
    private CommonRequestHeader header;
    private CommonRequestBody body;
    private CommonRequestExtendedHeader extendedHeader;
    private Boolean isSincronous;
    private String restfulMethod;
    private MessageContext messageContext;

    public TransactionRequest() {
        this.header = new CommonRequestHeader();
        this.body = new CommonRequestBody();
        this.isSincronous = Boolean.valueOf(true);
    }


    public TransactionRequest(String messageID, String uid, Boolean isSincronous, LogicalProtocol logicalProtocol, CommonRequestHeader header, CommonRequestBody body) {
        this.messageID = messageID;
        this.uid = uid;
        this.header = header;
        this.body = body;

        if (isSincronous == null) {
            this.isSincronous = Boolean.valueOf(true);
        } else {

            this.isSincronous = isSincronous;
        }
    }


    public TransactionRequest(String messageID, String uid, Boolean isSincronous, LogicalProtocol logicalProtocol, CommonRequestHeader header, CommonRequestBody body, CommonRequestExtendedHeader extendedHeader) {
        this.messageID = messageID;
        this.uid = uid;
        this.header = header;
        this.body = body;
        this.extendedHeader = extendedHeader;

        if (isSincronous == null) {
            this.isSincronous = Boolean.valueOf(true);
        } else {

            this.isSincronous = isSincronous;
        }
    }


    public TransactionRequest(String messageID, String uid, Boolean isSincronous, LogicalProtocol logicalProtocol, CommonRequestHeader header, CommonRequestBody body, CommonRequestExtendedHeader extendedHeader, PhysicalProtocol physicalProtocol) {
        this.messageID = messageID;
        this.uid = uid;
        this.header = header;
        this.body = body;
        this.extendedHeader = extendedHeader;

        if (isSincronous == null) {
            this.isSincronous = Boolean.valueOf(true);
        } else {

            this.isSincronous = isSincronous;
        }
    }


    public final String getMessageID() {
        return this.messageID;
    }


    public final void setMessageID(String messageID) {
        this.messageID = messageID;
    }


    public String getUid() {
        return this.uid;
    }


    public void setUid(String uid) {
        this.uid = uid;
    }


    public final LogicalProtocol getLogicalProtocol() {
        return this.logicalProtocol;
    }


    public final void setLogicalProtocol(LogicalProtocol logicalProtocol) {
        this.logicalProtocol = logicalProtocol;
    }


    public PhysicalProtocol getPhysicalProtocol() {
        return physicalProtocol;
    }


    public void setPhysicalProtocol(PhysicalProtocol physicalProtocol) {
        this.physicalProtocol = physicalProtocol;
    }


    public final CommonRequestHeader getHeader() {
        return this.header;
    }


    public final void setHeader(CommonRequestHeader header) {
        this.header = header;
    }


    public final CommonRequestBody getBody() {
        return this.body;
    }


    public final void setBody(CommonRequestBody body) {
        this.body = body;
    }


    public final CommonRequestExtendedHeader getExtendedHeader() {
        return this.extendedHeader;
    }


    public final void setExtendedHeader(CommonRequestExtendedHeader extendedHeader) {
        this.extendedHeader = extendedHeader;
    }


    public Boolean getIsSincronous() {
        return this.isSincronous;
    }


    public void setIsSincronous(Boolean isSincronous) {
        this.isSincronous = isSincronous;
    }


    public String toString() {
        return "TransactionRequest [messageID=" + this.messageID + ", uid=" + this.uid + ", logicalProtocol=" + this.logicalProtocol + ", header=" + this.header + ", body=" + this.body + "]";
    }


    public String getRestfulMethod() {
        return this.restfulMethod;
    }


    public void setRestfulMethod(String restfulMethod) {
        this.restfulMethod = restfulMethod;
    }


    public MessageContext getMessageContext() {
        return this.messageContext;
    }


    public void setMessageContext(MessageContext messageContext) {
        this.messageContext = messageContext;
    }

}
