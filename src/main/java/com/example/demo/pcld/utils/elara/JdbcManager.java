package com.example.demo.pcld.utils.elara;

import javax.sql.DataSource;

public interface JdbcManager {

    CommonJdbcTemplate getJdbcConnection(String paramString1, String paramString2);

    Boolean createCommonJdbcTemplate(String paramString1, String paramString2, DataSource paramDataSource, DataSourceInfo paramDataSourceInfo);

    Boolean deleteCommonJdbcTemplate(String paramString1, String paramString2);
}
