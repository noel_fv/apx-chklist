package com.example.demo.pcld.utils.elara;

/*

import com.bbva.elara.configuration.manager.ConfigurationManager;
import com.bbva.elara.dao.template.CommonJdbcTemplateImpl;
import com.bbva.elara.domain.datasource.DataSourceInfo;
import com.bbva.elara.domain.jdbc.CommonJdbcTemplate;
import com.bbva.elara.support.jdbc.manager.JdbcManager;

import java.util.concurrent.ConcurrentHashMap;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class JdbcManagerImpl
        implements JdbcManager {
    private ConfigurationManager configurationManager;
    private static final String MAX_ROWS = "query.maxrows";
    private static final Logger LOGGER = LoggerFactory.getLogger(JdbcManagerImpl.class);


    private ConcurrentHashMap<String, ConcurrentHashMap<String, CommonJdbcTemplate>> connectionsMap = new ConcurrentHashMap();


    public CommonJdbcTemplate getJdbcConnection(String uuaa, String bbdd) {
        LOGGER.debug("Get CommonJdbcTemplate for the uuaa '{}' with database name '{}'.", uuaa, bbdd);
        return (CommonJdbcTemplate) ((ConcurrentHashMap) this.connectionsMap.get(uuaa)).get(bbdd);
    }


    public Boolean createCommonJdbcTemplate(String uuaa, String dbName, DataSource datasource, DataSourceInfo dataSourceInfo) {
        Boolean changed = Boolean.valueOf(false);
        uuaa = uuaa.toLowerCase();

        int maxRows = Integer.parseInt(this.configurationManager.getProperty("query.maxrows"));
        CommonJdbcTemplateImpl commonJdbcTemplateImpl = new CommonJdbcTemplateImpl(datasource, maxRows);

        ConcurrentHashMap<String, CommonJdbcTemplate> dbTemplate = (ConcurrentHashMap) this.connectionsMap.get(uuaa);
        if (dbTemplate != null) {
            dbTemplate.put(dbName, commonJdbcTemplateImpl);
            this.connectionsMap.put(uuaa, dbTemplate);
            LOGGER.debug("Updated CommonJdbcTemplate for the uuaa '{}' with database name '{}'.", uuaa, dbName);
        } else {

            ConcurrentHashMap<String, CommonJdbcTemplate> newDbTemplate = new ConcurrentHashMap<String, CommonJdbcTemplate>();
            CommonJdbcTemplateImpl commonJdbcTemplateImpl1 = new CommonJdbcTemplateImpl(datasource, maxRows);
            newDbTemplate.put(dbName, commonJdbcTemplateImpl1);
            this.connectionsMap.put(uuaa, newDbTemplate);
            LOGGER.debug("Created CommonJdbcTemplate for the uuaa '{}' with database name '{}'.", uuaa, dbName);
            changed = Boolean.valueOf(true);
        }

        return changed;
    }


    public Boolean deleteCommonJdbcTemplate(String uuaa, String dbName) {
        Boolean deleted = Boolean.valueOf(false);
        uuaa = uuaa.toLowerCase();

        if (this.connectionsMap.containsKey(uuaa)) {
            ((ConcurrentHashMap) this.connectionsMap.get(uuaa)).remove(dbName);
            deleted = Boolean.valueOf(true);
            LOGGER.debug("Deleted CommonJdbcTemplate for the uuaa '{}' with database name '{}'.", uuaa, dbName);
        }

        return deleted;
    }


    public void setConfigurationManager(ConfigurationManager configurationManager) {
        this.configurationManager = configurationManager;
    }
}
*/