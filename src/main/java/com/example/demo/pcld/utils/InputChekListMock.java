package com.example.demo.pcld.utils;

import com.example.demo.pcld.dtos.input.InputChekList;
import com.fasterxml.jackson.core.JsonProcessingException;
//import org.codehaus.jackson.map.DeserializationConfig;
//import org.codehaus.jackson.map.ObjectMapper;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.IOException;
import java.io.InputStream;


public class InputChekListMock {

	private static final InputChekListMock INSTANCE = new InputChekListMock();

	private ObjectMapper objectMapper;

	private InputChekListMock() {
		objectMapper = new ObjectMapper();
		objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
		//objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
	}

	public static InputChekListMock getInstance() {
		return INSTANCE;
	}

	public InputChekList getInputChekList() throws IOException {
		try  {
			InputStream is = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("mock/inputChkList.json");

			return objectMapper.readValue(is, InputChekList.class);
		}catch (IOException e){
			throw e;
		}
	}
	  String writeValueAsString(InputChekList inputChekList) throws JsonProcessingException {
		return objectMapper.writeValueAsString(inputChekList);
	}

	public static void main(String[] args) throws Exception{
		InputChekList input=getInstance().getInputChekList();
		System.out.println(getInstance().writeValueAsString(input));
	}

}
