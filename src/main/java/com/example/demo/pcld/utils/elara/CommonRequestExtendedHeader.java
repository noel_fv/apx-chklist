package com.example.demo.pcld.utils.elara;


import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;





public class CommonRequestExtendedHeader
        implements Serializable
{
    private static final long serialVersionUID = 1L;
    private Map<RequestExtendedHeaderParamsName, Object> extendedHeaderParamsMap;

    public CommonRequestExtendedHeader() {
        this.extendedHeaderParamsMap = new HashMap();






        this.extendedHeaderParamsMap.put(RequestExtendedHeaderParamsName.ACCOUNTINGTERMINAL, null);
        this.extendedHeaderParamsMap.put(RequestExtendedHeaderParamsName.LOGICALTERMINAL, null);
        this.extendedHeaderParamsMap.put(RequestExtendedHeaderParamsName.SEQUENCEID, null);
    }




    public CommonRequestExtendedHeader(String accountingTerminal, String logicalTerminal, String sequenceID) {
        this.extendedHeaderParamsMap = new HashMap();
        this.extendedHeaderParamsMap.put(RequestExtendedHeaderParamsName.ACCOUNTINGTERMINAL, accountingTerminal);
        this.extendedHeaderParamsMap.put(RequestExtendedHeaderParamsName.LOGICALTERMINAL, logicalTerminal);
        this.extendedHeaderParamsMap.put(RequestExtendedHeaderParamsName.SEQUENCEID, sequenceID);
    }









    public Object getExtendedHeaderParameter(RequestExtendedHeaderParamsName extendedHeaderParamsName) { return this.extendedHeaderParamsMap.get(extendedHeaderParamsName); }






    public final String getAccountingTerminal() { return (String)this.extendedHeaderParamsMap.get(RequestExtendedHeaderParamsName.ACCOUNTINGTERMINAL); }







    public final void setAccountingTerminal(String accountingTerminal) { this.extendedHeaderParamsMap.put(RequestExtendedHeaderParamsName.ACCOUNTINGTERMINAL, accountingTerminal); }






    public final String getLogicalTerminal() { return (String)this.extendedHeaderParamsMap.get(RequestExtendedHeaderParamsName.LOGICALTERMINAL); }







    public final void setLogicalTerminal(String logicalTerminal) { this.extendedHeaderParamsMap.put(RequestExtendedHeaderParamsName.LOGICALTERMINAL, logicalTerminal); }






    public final String getSequenceID() { return (String)this.extendedHeaderParamsMap.get(RequestExtendedHeaderParamsName.SEQUENCEID); }







    public final void setSequenceID(String sequenceID) { this.extendedHeaderParamsMap.put(RequestExtendedHeaderParamsName.SEQUENCEID, sequenceID); }




    public String toString() { return "CommonExtendedRequestHeader [extendedHeaderParamsMap=" + this.extendedHeaderParamsMap + "]"; }
}
