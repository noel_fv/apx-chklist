package com.example.demo.pcld.utils.elara;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ParameterTable
        implements Serializable {
    private static final Logger LOGGER = LoggerFactory.getLogger(ParameterTable.class);


    private static final long serialVersionUID = 1L;


    private List<String> columnNames;


    private List<Map<String, Object>> parameterTableList;


    public ParameterTable(List<String> columnNames) {
        this.parameterTableList = new ArrayList();
        this.columnNames = new ArrayList(columnNames);
    }


    public ParameterTable(Set<String> columnNames) {
        this.parameterTableList = new ArrayList();
        this.columnNames = new ArrayList();
        this.columnNames.addAll(columnNames);
    }


    public ParameterTable() {
        this.parameterTableList = new ArrayList();
        this.columnNames = new ArrayList();
    }


    public boolean add(Map<String, Object> arg0) {
        Set<String> columnSet = arg0.keySet();
        for (String columnName : columnSet) {
            if (!this.columnNames.contains(columnName)) {
                LOGGER.error("ColumnName " + columnName + " has not been defined for the ParameterTable");
                return false;
            }
        }
        return this.parameterTableList.add(arg0);
    }


    public void clear() {
        this.parameterTableList.clear();
    }


    public boolean contains(Object arg0) {
        return this.parameterTableList.contains(arg0);
    }


    public Map<String, Object> get(int arg0) {
        return (Map) this.parameterTableList.get(arg0);
    }


    public Map<String, Object> get(String columnName, Object columnValue) {
        for (Map<String, Object> map : this.parameterTableList) {
            if (map.containsKey(columnName)) {
                Object object = map.get(columnName);
                if (object.equals(columnValue)) {
                    return map;
                }
            }
        }
        return null;
    }


    public boolean isEmpty() {
        return this.parameterTableList.isEmpty();
    }


    public Iterator<Map<String, Object>> iterator() {
        return this.parameterTableList.iterator();
    }


    public boolean remove(Object arg0) {
        return this.parameterTableList.remove(arg0);
    }


    public Map<String, Object> remove(int arg0) {
        return (Map) this.parameterTableList.remove(arg0);
    }


    public int size() {
        return this.parameterTableList.size();
    }


    public final List<String> getColumnNames() {
        return this.columnNames;
    }


    public void setColumnNames(List<String> columnNames) {
        this.columnNames = columnNames;
    }


    public final void setParameterTableList(List<Map<String, Object>> parameterTableList) {
        if (parameterTableList != null) {
            this.parameterTableList.addAll(parameterTableList);
            if (!parameterTableList.isEmpty()) {
                this.columnNames.addAll(((Map) parameterTableList.get(0)).keySet());
            }
        }
    }


    public List<Map<String, Object>> getParameterTableList() {
        return this.parameterTableList;
    }


    public String toString() {
        return this.parameterTableList.toString();
    }


    public boolean equals(Object parameterTable) {
        if (this == parameterTable) return true;
        if (parameterTable == null || getClass() != parameterTable.getClass()) return false;

        ParameterTable that = (ParameterTable) parameterTable;

        if ((this.columnNames != null) ? !this.columnNames.equals(that.columnNames) : (that.columnNames != null))
            return false;
        return true;
    }


    public int hashCode() {
        int result = (this.columnNames != null) ? this.columnNames.hashCode() : 0;
        return 31 * result + ((this.parameterTableList != null) ? this.parameterTableList.hashCode() : 0);
    }
}

