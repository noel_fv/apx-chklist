package com.example.demo.pcld.utils.elara;

import java.io.Serializable;


public class VariableRopInfo
        implements Serializable {
    private static final long serialVersionUID = 1L;
    private String outputCopyCode;
    private String additionalInfo;

    public VariableRopInfo() {
    }

    public VariableRopInfo(String outputCopyCode, String additionalInfo) {
        this.outputCopyCode = outputCopyCode;
        this.additionalInfo = additionalInfo;
    }


    public String getOutputCopyCode() {
        return this.outputCopyCode;
    }


    public void setOutputCopyCode(String outputCopyCode) {
        this.outputCopyCode = outputCopyCode;
    }


    public String getAdditionalInfo() {
        return this.additionalInfo;
    }


    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }




    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        VariableRopInfo other = (VariableRopInfo) obj;
        if (this.additionalInfo == null) {
            if (other.additionalInfo != null)
                return false;
        } else if (!this.additionalInfo.equals(other.additionalInfo)) {
            return false;
        }
        if (this.outputCopyCode == null) {
            if (other.outputCopyCode != null)
                return false;
        } else if (!this.outputCopyCode.equals(other.outputCopyCode)) {
            return false;
        }
        return true;
    }


    public String toString() {
        return "VariableRopInfo [outputCopyCode=" + this.outputCopyCode + ", additionalInfo=" + this.additionalInfo + "]";
    }
}
