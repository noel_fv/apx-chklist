package com.example.demo.pcld.utils.elara;


import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;


public class LotesRopInfo
        implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer relatedUid;
    private String ibanOrigin;
    private String origAccountOperation;
    private String origCountryAccount;
    private String ibanDestiny;
    private String destAccountOperation;
    private String destCountryAccount;
    private String customerType;
    private String customerDocument;
    private String currencyCode;
    private BigDecimal amount;
    private String referenceType1;
    private String reference1;
    private String referenceType2;
    private String reference2;
    private Date operationDate;
    private String additionalInfo;
    private BigDecimal rmsCode;
    private String currencyOriginalCode;
    private BigDecimal totalAmmountRms;
    private String interestRmsCode;
    private BigDecimal totalInterestRms;
    private String commisionRmsCode;
    private BigDecimal totalCommisionRms;
    private String expensesRmsCode;
    private BigDecimal totalExpensesRms;
    private String programCode;
    private String customerCode;

    public LotesRopInfo() {
    }

    public LotesRopInfo(Integer relatedUid, String ibanOrigin, String origAccountOperation, String origCountryAccount, String ibanDestiny, String destAccountOperation, String destCountryAccount, String customerType, String customerDocument, String currencyCode, BigDecimal amount, String referenceType1, String reference1, String referenceType2, String reference2, Date operationDate, String additionalInfo, BigDecimal rmsCode, String currencyOriginalCode, BigDecimal totalAmmountRms, String interestRmsCode, BigDecimal totalInterestRms, String commisionRmsCode, BigDecimal totalCommisionRms, String expensesRmsCode, BigDecimal totalExpensesRms, String programCode, String customerCode) {
        this.relatedUid = relatedUid;
        this.ibanOrigin = ibanOrigin;
        this.origAccountOperation = origAccountOperation;
        this.origCountryAccount = origCountryAccount;
        this.ibanDestiny = ibanDestiny;
        this.destAccountOperation = destAccountOperation;
        this.destCountryAccount = destCountryAccount;
        this.customerType = customerType;
        this.customerDocument = customerDocument;
        this.currencyCode = currencyCode;
        this.amount = amount;
        this.referenceType1 = referenceType1;
        this.reference1 = reference1;
        this.referenceType2 = referenceType2;
        this.reference2 = reference2;
        this.operationDate = operationDate;
        this.additionalInfo = additionalInfo;
        this.rmsCode = rmsCode;
        this.currencyOriginalCode = currencyOriginalCode;
        this.totalAmmountRms = totalAmmountRms;
        this.interestRmsCode = interestRmsCode;
        this.totalInterestRms = totalInterestRms;
        this.commisionRmsCode = commisionRmsCode;
        this.totalCommisionRms = totalCommisionRms;
        this.expensesRmsCode = expensesRmsCode;
        this.totalExpensesRms = totalExpensesRms;
        this.programCode = programCode;
        this.customerCode = customerCode;
    }


    public Integer getRelatedUid() {
        return this.relatedUid;
    }


    public void setRelatedUid(Integer relatedUid) {
        this.relatedUid = relatedUid;
    }


    public String getIbanOrigin() {
        return this.ibanOrigin;
    }


    public void setIbanOrigin(String ibanOrigin) {
        this.ibanOrigin = ibanOrigin;
    }


    public String getOrigAccountOperation() {
        return this.origAccountOperation;
    }


    public void setOrigAccountOperation(String origAccountOperation) {
        this.origAccountOperation = origAccountOperation;
    }


    public String getOrigCountryAccount() {
        return this.origCountryAccount;
    }


    public void setOrigCountryAccount(String origCountryAccount) {
        this.origCountryAccount = origCountryAccount;
    }


    public String getIbanDestiny() {
        return this.ibanDestiny;
    }


    public void setIbanDestiny(String ibanDestiny) {
        this.ibanDestiny = ibanDestiny;
    }


    public String getDestAccountOperation() {
        return this.destAccountOperation;
    }


    public void setDestAccountOperation(String destAccountOperation) {
        this.destAccountOperation = destAccountOperation;
    }


    public String getDestCountryAccount() {
        return this.destCountryAccount;
    }


    public void setDestCountryAccount(String destCountryAccount) {
        this.destCountryAccount = destCountryAccount;
    }


    public String getCustomerType() {
        return this.customerType;
    }


    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }


    public String getCustomerDocument() {
        return this.customerDocument;
    }


    public void setCustomerDocument(String customerDocument) {
        this.customerDocument = customerDocument;
    }


    public String getCurrencyCode() {
        return this.currencyCode;
    }


    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }


    public BigDecimal getAmount() {
        return this.amount;
    }


    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }


    public String getReferenceType1() {
        return this.referenceType1;
    }


    public void setReferenceType1(String referenceType1) {
        this.referenceType1 = referenceType1;
    }


    public String getReference1() {
        return this.reference1;
    }


    public void setReference1(String reference1) {
        this.reference1 = reference1;
    }


    public String getReferenceType2() {
        return this.referenceType2;
    }


    public void setReferenceType2(String referenceType2) {
        this.referenceType2 = referenceType2;
    }


    public String getReference2() {
        return this.reference2;
    }


    public void setReference2(String reference2) {
        this.reference2 = reference2;
    }


    public Date getOperationDate() {
        return this.operationDate;
    }


    public void setOperationDate(Date operationDate) {
        this.operationDate = operationDate;
    }


    public String getAdditionalInfo() {
        return this.additionalInfo;
    }


    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }


    public BigDecimal getRmsCode() {
        return this.rmsCode;
    }


    public void setRmsCode(BigDecimal rmsCode) {
        this.rmsCode = rmsCode;
    }


    public String getCurrencyOriginalCode() {
        return this.currencyOriginalCode;
    }


    public void setCurrencyOriginalCode(String currencyOriginalCode) {
        this.currencyOriginalCode = currencyOriginalCode;
    }


    public BigDecimal getTotalAmmountRms() {
        return this.totalAmmountRms;
    }


    public void setTotalAmmountRms(BigDecimal totalAmmountRms) {
        this.totalAmmountRms = totalAmmountRms;
    }


    public String getInterestRmsCode() {
        return this.interestRmsCode;
    }


    public void setInterestRmsCode(String interestRmsCode) {
        this.interestRmsCode = interestRmsCode;
    }


    public BigDecimal getTotalInterestRms() {
        return this.totalInterestRms;
    }


    public void setTotalInterestRms(BigDecimal totalInterestRms) {
        this.totalInterestRms = totalInterestRms;
    }


    public String getCommisionRmsCode() {
        return this.commisionRmsCode;
    }


    public void setCommisionRmsCode(String commisionRmsCode) {
        this.commisionRmsCode = commisionRmsCode;
    }


    public BigDecimal getTotalCommisionRms() {
        return this.totalCommisionRms;
    }


    public void setTotalCommisionRms(BigDecimal totalCommisionRms) {
        this.totalCommisionRms = totalCommisionRms;
    }


    public String getExpensesRmsCode() {
        return this.expensesRmsCode;
    }


    public void setExpensesRmsCode(String expensesRmsCode) {
        this.expensesRmsCode = expensesRmsCode;
    }


    public BigDecimal getTotalExpensesRms() {
        return this.totalExpensesRms;
    }


    public void setTotalExpensesRms(BigDecimal totalExpensesRms) {
        this.totalExpensesRms = totalExpensesRms;
    }


    public String getProgramCode() {
        return this.programCode;
    }


    public void setProgramCode(String programCode) {
        this.programCode = programCode;
    }


    public String getCustomerCode() {
        return this.customerCode;
    }


    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }


    public String toString() {
        return "LotesRopInfo [relatedUid=" + this.relatedUid + ", ibanOrigin=" + this.ibanOrigin + ", origAccountOperation=" + this.origAccountOperation + ", origCountryAccount=" + this.origCountryAccount + ", ibanDestiny=" + this.ibanDestiny + ", destAccountOperation=" + this.destAccountOperation + ", destCountryAccount=" + this.destCountryAccount + ", customerType=" + this.customerType + ", customerDocument=" + this.customerDocument + ", currencyCode=" + this.currencyCode + ", amount=" + this.amount + ", referenceType1=" + this.referenceType1 + ", reference1=" + this.reference1 + ", referenceType2=" + this.referenceType2 + ", reference2=" + this.reference2 + ", operationDate=" + this.operationDate + ", additionalInfo=" + this.additionalInfo + ", rmsCode=" + this.rmsCode + ", currencyOriginalCode=" + this.currencyOriginalCode + ", totalAmmountRms=" + this.totalAmmountRms + ", interestRmsCode=" + this.interestRmsCode + ", totalInterestRms=" + this.totalInterestRms + ", commisionRmsCode=" + this.commisionRmsCode + ", totalCommisionRms=" + this.totalCommisionRms + ", expensesRmsCode=" + this.expensesRmsCode + ", totalExpensesRms=" + this.totalExpensesRms + ", programCode=" + this.programCode + ", customerCode=" + this.customerCode + "]";
    }
}
