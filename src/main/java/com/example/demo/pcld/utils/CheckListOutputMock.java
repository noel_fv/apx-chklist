package com.example.demo.pcld.utils;

import com.example.demo.pcld.dtos.output.OutputChekList;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.IOException;
import java.io.InputStream;


public class CheckListOutputMock {

	private static final CheckListOutputMock INSTANCE = new CheckListOutputMock();

	private ObjectMapper objectMapper;

	private CheckListOutputMock() {
		objectMapper = new ObjectMapper();
		objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
		//objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
	}

	public static CheckListOutputMock getInstance() {
		return INSTANCE;
	}

	public OutputChekList getOutputChekList() {
		OutputChekList outputChekList=new OutputChekList();
		try  {
			InputStream is = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("mock/outputChkList.json");

			 outputChekList= objectMapper.readValue(is, OutputChekList.class);
		}catch (IOException e){
			e.printStackTrace();
		}
		return outputChekList;
	}

	  public String writeValueAsString(OutputChekList outputChekList){
		  String json="";
		  try  {
			  json=objectMapper.writeValueAsString(outputChekList);
		  }catch (JsonProcessingException e){
			  e.printStackTrace();
		  }
		return json;
	}

	public static void main(String[] args) throws Exception{
		OutputChekList input= getInstance().getOutputChekList();
		System.out.println(getInstance().writeValueAsString(input));

	}

}
