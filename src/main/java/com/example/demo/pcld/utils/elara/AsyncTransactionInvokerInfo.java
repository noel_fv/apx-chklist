package com.example.demo.pcld.utils.elara;

import java.io.Serializable;
import java.util.Map;


public class AsyncTransactionInvokerInfo
        implements Serializable {
    private static final long serialVersionUID = -1568168167270602384L;
    private Map<String, Object> body;
    private String logicalTransactionCode;
    private String versionCode;

    public AsyncTransactionInvokerInfo(Map<String, Object> body, String logicalTransactionCode, String versionCode) {
        this.body = body;
        this.logicalTransactionCode = logicalTransactionCode;
        this.versionCode = versionCode;
    }


    public Map<String, Object> getBody() {
        return this.body;
    }


    public void setBody(Map<String, Object> body) {
        this.body = body;
    }


    public String getLogicalTransactionCode() {
        return this.logicalTransactionCode;
    }


    public void setLogicalTransactionCode(String logicalTransactionCode) {
        this.logicalTransactionCode = logicalTransactionCode;
    }


    public String getVersionCode() {
        return this.versionCode;
    }


    public void setVersionCode(String versionCode) {
        this.versionCode = versionCode;
    }


    public String toString() {
        return "AsyncTransactionInvokerInfo [body=" + this.body + ", logicalTransactionCode=" + this.logicalTransactionCode + ", versionCode=" + this.versionCode + "]";
    }
}
