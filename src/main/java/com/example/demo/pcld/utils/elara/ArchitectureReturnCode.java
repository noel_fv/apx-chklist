package com.example.demo.pcld.utils.elara;


public   enum ArchitectureReturnCode {
    NOROLLBACK("16"),


    ROLLBACK("18");


    private String value;


    ArchitectureReturnCode(String value) {
        this.value = value;
    }


    public String getValue() {
        return this.value;
    }
}
