package com.example.demo.pcld.utils.elara;


import java.io.Serializable;

public class AccountingTransactionData
        implements Serializable {
    private static final long serialVersionUID = 1L;
    private String originAccount;
    private String targetAccount;
    private String originCountry;
    private String targetCountry;
    private String clientID;
    private String clientType;
    private String desReferenc2;

    public String toString() {
        return "AccountingTransactionData [amount=" + this.amount + ", checkNumber=" + this.checkNumber + ", clientID=" + this.clientID + ", clientType=" + this.clientType + ", creditCardNumber=" + this.creditCardNumber + ", currencyCode=" + this.currencyCode + ", debitCredit=" + this.debitCredit + ", desReferenc=" + this.desReferenc + ", desReferenc2=" + this.desReferenc2 + ", flagTotal=" + this.flagTotal + ", originAccount=" + this.originAccount + ", originCountry=" + this.originCountry + ", originTypeAccount=" + this.originTypeAccount + ", targetAccount=" + this.targetAccount + ", targetCountry=" + this.targetCountry + ", targetTypeAccount=" + this.targetTypeAccount + "]";
    }


    private String desReferenc;


    private String checkNumber;


    private String creditCardNumber;


    private String flagTotal;


    private String currencyCode;


    private Double amount;


    private String debitCredit;


    private String originTypeAccount;


    private String targetTypeAccount;


    public AccountingTransactionData() {
    }


    public AccountingTransactionData(String originAccount, String targetAccount, String originCountry, String targetCountry, String clientID, String typeClientIdent, String desReferenc2, String desReferenc, String checkNumber, String creditCardNumber, String flagTotal, String currencyCode, Double amount, String debitCredit, String originTypeAccount, String targetTypeAccount) {
        this.originAccount = originAccount;
        this.targetAccount = targetAccount;
        this.originCountry = originCountry;
        this.targetCountry = targetCountry;
        this.clientID = clientID;
        this.clientType = typeClientIdent;
        this.desReferenc2 = desReferenc2;
        this.desReferenc = desReferenc;
        this.checkNumber = checkNumber;
        this.creditCardNumber = creditCardNumber;
        this.flagTotal = flagTotal;
        this.currencyCode = currencyCode;
        this.amount = amount;
        this.debitCredit = debitCredit;
        this.originTypeAccount = originTypeAccount;
        this.targetTypeAccount = targetTypeAccount;
    }


    public String getOriginAccount() {
        return this.originAccount;
    }


    public void setOriginAccount(String originAccount) {
        this.originAccount = originAccount;
    }


    public String getTargetAccount() {
        return this.targetAccount;
    }


    public void setTargetAccount(String targetAccount) {
        this.targetAccount = targetAccount;
    }


    public String getOriginCountry() {
        return this.originCountry;
    }


    public void setOriginCountry(String originCountry) {
        this.originCountry = originCountry;
    }


    public String getTargetCountry() {
        return this.targetCountry;
    }


    public void setTargetCountry(String targetCountry) {
        this.targetCountry = targetCountry;
    }


    public String getClientID() {
        return this.clientID;
    }


    public void setClientID(String clientID) {
        this.clientID = clientID;
    }


    public String getClientType() {
        return this.clientType;
    }


    public void setClientType(String typeClientIdent) {
        this.clientType = typeClientIdent;
    }


    public String getDesReferenc2() {
        return this.desReferenc2;
    }


    public void setDesReferenc2(String desReferenc2) {
        this.desReferenc2 = desReferenc2;
    }


    public String getDesReferenc() {
        return this.desReferenc;
    }


    public void setDesReferenc(String desReferenc) {
        this.desReferenc = desReferenc;
    }


    public String getCheckNumber() {
        return this.checkNumber;
    }


    public void setCheckNumber(String checkNumber) {
        this.checkNumber = checkNumber;
    }


    public String getCreditCardNumber() {
        return this.creditCardNumber;
    }


    public void setCreditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }


    public String getFlagTotal() {
        return this.flagTotal;
    }


    public void setFlagTotal(String flagTotal) {
        this.flagTotal = flagTotal;
    }


    public String getCurrencyCode() {
        return this.currencyCode;
    }


    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }


    public Double getAmount() {
        return this.amount;
    }


    public void setAmount(Double amount) {
        this.amount = amount;
    }


    public String getDebitCredit() {
        return this.debitCredit;
    }


    public void setDebitCredit(String debitCredit) {
        this.debitCredit = debitCredit;
    }


    public String getOriginTypeAccount() {
        return this.originTypeAccount;
    }


    public void setOriginTypeAccount(String originTypeAccount) {
        this.originTypeAccount = originTypeAccount;
    }


    public String getTargetTypeAccount() {
        return this.targetTypeAccount;
    }


    public void setTargetTypeAccount(String targetTypeAccount) {
        this.targetTypeAccount = targetTypeAccount;
    }


}
