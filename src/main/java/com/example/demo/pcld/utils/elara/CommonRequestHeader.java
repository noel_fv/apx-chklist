package com.example.demo.pcld.utils.elara;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


public class CommonRequestHeader
        implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final int START_APPLICATION_CODE = 0;
    private static final int START_TRANSACTION_CODE = 4;
    private Map<RequestHeaderParamsName, Object> headerParamsMap;

    public CommonRequestHeader() {
        this.headerParamsMap = new HashMap();


        this.headerParamsMap.put(RequestHeaderParamsName.PID, null);
        this.headerParamsMap.put(RequestHeaderParamsName.LOGICALTRANSACTIONCODE, null);
        this.headerParamsMap.put(RequestHeaderParamsName.USERCODE, null);
        this.headerParamsMap.put(RequestHeaderParamsName.LANGUAGECODE, null);
        this.headerParamsMap.put(RequestHeaderParamsName.APPLICATIONCODE, null);
        this.headerParamsMap.put(RequestHeaderParamsName.TRANSACTIONCODE, null);
        this.headerParamsMap.put(RequestHeaderParamsName.VERSIONCODE, null);
    }


    public CommonRequestHeader(String pid, String logicalTransactionCode, String userCode, String languageCode, String versionCode) {
        this.headerParamsMap = new HashMap();
        this.headerParamsMap.put(RequestHeaderParamsName.PID, pid);
        this.headerParamsMap.put(RequestHeaderParamsName.LOGICALTRANSACTIONCODE, logicalTransactionCode);
        this.headerParamsMap.put(RequestHeaderParamsName.USERCODE, userCode);
        this.headerParamsMap.put(RequestHeaderParamsName.LANGUAGECODE, languageCode);
        this.headerParamsMap.put(RequestHeaderParamsName.VERSIONCODE, versionCode);
        if (logicalTransactionCode != null) {
            this.headerParamsMap.put(RequestHeaderParamsName.APPLICATIONCODE, logicalTransactionCode
                    .substring(0, 4));

            this.headerParamsMap.put(RequestHeaderParamsName.TRANSACTIONCODE, logicalTransactionCode

                    .substring(4));
        }
    }


    public Object getHeaderParameter(RequestHeaderParamsName headerParamsName) {
        return this.headerParamsMap.get(headerParamsName);
    }


    public final String getPid() {
        return (String) this.headerParamsMap.get(RequestHeaderParamsName.PID);
    }


    public final void setPid(String pid) {
        this.headerParamsMap.put(RequestHeaderParamsName.PID, pid);
    }


    public final String getLogicalTransactionCode() {
        return (String) this.headerParamsMap.get(RequestHeaderParamsName.LOGICALTRANSACTIONCODE);
    }


    public final void setLogicalTransactionCode(String logicalTransactionCode) {
        this.headerParamsMap.put(RequestHeaderParamsName.LOGICALTRANSACTIONCODE, logicalTransactionCode);
        if (logicalTransactionCode != null) {
            this.headerParamsMap.put(RequestHeaderParamsName.APPLICATIONCODE, logicalTransactionCode
                    .substring(0, 4));

            this.headerParamsMap.put(RequestHeaderParamsName.TRANSACTIONCODE, logicalTransactionCode

                    .substring(4));
        }
    }


    public final String getApplicationCode() {
        return (String) this.headerParamsMap.get(RequestHeaderParamsName.APPLICATIONCODE);
    }


    public final void setApplicationCode(String applicationCode) {
        this.headerParamsMap.put(RequestHeaderParamsName.APPLICATIONCODE, applicationCode);
    }


    public final String getTransactionCode() {
        return (String) this.headerParamsMap.get(RequestHeaderParamsName.TRANSACTIONCODE);
    }


    public final void setTransactionCode(String transactionCode) {
        this.headerParamsMap.put(RequestHeaderParamsName.TRANSACTIONCODE, transactionCode);
    }


    public final String getUserCode() {
        return (String) this.headerParamsMap.get(RequestHeaderParamsName.USERCODE);
    }


    public final void setUserCode(String userCode) {
        this.headerParamsMap.put(RequestHeaderParamsName.USERCODE, userCode);
    }


    public final String getLanguageCode() {
        return (String) this.headerParamsMap.get(RequestHeaderParamsName.LANGUAGECODE);
    }


    public final void setLanguageCode(String languageCode) {
        this.headerParamsMap.put(RequestHeaderParamsName.LANGUAGECODE, languageCode);
    }


    public final String getVersionCode() {
        return (String) this.headerParamsMap.get(RequestHeaderParamsName.VERSIONCODE);
    }


    public final void setVersionCode(String versionCode) {
        this.headerParamsMap.put(RequestHeaderParamsName.VERSIONCODE, versionCode);
    }


    public String toString() {
        return "CommonRequestHeader [headerParamsMap=" + this.headerParamsMap + "]";
    }
}
