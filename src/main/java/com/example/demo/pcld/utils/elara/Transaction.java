package com.example.demo.pcld.utils.elara;

public interface Transaction {
    public static final String KEY_DIC_TXN = "transactionName";

    void execute();

    void setTransactionName(String paramString);

    String getTransactionName();

    String getUid();

    void setUid(String paramString);

    void setContext(Context paramContext);

    Context getContext();
}
