package com.example.demo.pcld.utils.elara;

public enum RequestExtendedHeaderParamsName {
    SEQUENCEID, LOGICALTERMINAL, ACCOUNTINGTERMINAL;
}
