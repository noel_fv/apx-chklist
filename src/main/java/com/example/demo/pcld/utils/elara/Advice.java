package com.example.demo.pcld.utils.elara;

import java.io.Serializable;
import java.util.Arrays;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;


public class Advice
        implements Serializable {
    private static final long serialVersionUID = 1L;
    private final String LIBRARY_PATTERN = "\\bcom\\b\\.\\bbbva\\b\\.\\b\\w{4}\\b\\.\\blib\\b\\.\\b\\w{4}\\b\\.\\bimpl\\b\\.\\b\\w{5,}\\b";


    private final String TRANSACTION_PATTERN = "com\\.bbva\\.\\w{4}\\.\\w{12}Transaction\\b";


    private String code;


    private String description;


    private String uuaa;


    private String onlyCode;


    private AdviceType type;


    private Object[] args;


    private String generatorClassName;


    private static final int startIndexUUAA = 0;


    private static final int startIndexCode = 4;


    public Advice() {
        this.generatorClassName = getGeneratorClassNameFromStackTrace();
    }


    public Advice(String code, String description, AdviceType type, Object[] args) {
        this.code = code;
        this.description = description;
        this.type = type;
        this.args = args;
        if (code != null) {
            this.uuaa = obtainAdviceUuaa(code);
            this.onlyCode = obtainJustCode(code);
        }
        this.generatorClassName = getGeneratorClassNameFromStackTrace();
    }


    public Advice(String code, String description, AdviceType type, String generatorClassName, Object[] args) {
        this.code = code;
        this.description = description;
        this.type = type;
        this.args = args;
        if (code != null) {
            this.uuaa = obtainAdviceUuaa(code);
            this.onlyCode = obtainJustCode(code);
        }
        this.generatorClassName = getGeneratorClassNameFromStackTrace();
    }


    private String obtainJustCode(String adviceCode) {
        return adviceCode.substring(4);
    }


    private String obtainAdviceUuaa(String adviceCode) {
        int size = 4;
        if (adviceCode.length() >= size) {
            return adviceCode.substring(0, 4);
        }
        return "";
    }


    private String getGeneratorClassNameFromStackTrace() {
        StackTraceElement[] trace = Thread.currentThread().getStackTrace();
        StackTraceElement invoke = null;

        for (StackTraceElement traceElement : trace) {
            if (Pattern.matches("\\bcom\\b\\.\\bbbva\\b\\.\\b\\w{4}\\b\\.\\blib\\b\\.\\b\\w{4}\\b\\.\\bimpl\\b\\.\\b\\w{5,}\\b", traceElement.getClassName().toString()) ||
                    Pattern.matches("com\\.bbva\\.\\w{4}\\.\\w{12}Transaction\\b", traceElement.getClassName().toString())) {
                invoke = traceElement;

                break;
            }
        }
        if (invoke != null) {
            StringBuilder component = new StringBuilder();
            component.append(getClassName(invoke.getClassName()).substring(0, 8));
            return component.toString();
        }
        return "architectureClass";
    }


    private String getClassName(String name) {
        String libName = StringUtils.substringAfterLast(name, ".");
        return libName.replace("Impl", "");
    }


    public String getCode() {
        return this.code;
    }


    public void setCode(String code) {
        this.code = code;
    }


    public String getDescription() {
        return this.description;
    }


    public void setDescription(String description) {
        this.description = description;
    }


    public AdviceType getType() {
        return this.type;
    }


    public void setType(AdviceType type) {
        this.type = type;
    }


    public String getUuaa() {
        return this.uuaa;
    }


    public void setUuaa(String uuaa) {
        this.uuaa = uuaa;
    }


    public String getOnlyCode() {
        return this.onlyCode;
    }


    public void setOnlyCode(String onlyCode) {
        this.onlyCode = onlyCode;
    }


    public Object[] getArgs() {
        return this.args;
    }


    public void setArgs(Object[] args) {
        this.args = args;
    }


    public String getGeneratorClassName() {
        return this.generatorClassName;
    }


    public String toString() {
        return "Advice [code=" + this.code + ", description=" + this.description + ", uuaa=" + this.uuaa + ", onlyCode=" + this.onlyCode + ", type=" + this.type + ", args=" +

                Arrays.toString(this.args) + ", generatorClassName=" + this.generatorClassName + "]";
    }


    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Advice other = (Advice) obj;
        if (!Arrays.equals(this.args, other.args))
            return false;
        if (this.code == null) {
            if (other.code != null)
                return false;
        } else if (!this.code.equals(other.code)) {
            return false;
        }
        if (this.description == null) {
            if (other.description != null)
                return false;
        } else if (!this.description.equals(other.description)) {
            return false;
        }
        if (this.generatorClassName == null) {
            if (other.generatorClassName != null)
                return false;
        } else if (!this.generatorClassName.equals(other.generatorClassName)) {
            return false;
        }
        if (this.onlyCode == null) {
            if (other.onlyCode != null)
                return false;
        } else if (!this.onlyCode.equals(other.onlyCode)) {
            return false;
        }
        if (this.type != other.type)
            return false;
        if (this.uuaa == null) {
            if (other.uuaa != null)
                return false;
        } else if (!this.uuaa.equals(other.uuaa)) {
            return false;
        }
        return true;
    }
}
