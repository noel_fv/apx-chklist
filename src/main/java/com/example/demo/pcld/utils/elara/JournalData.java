package com.example.demo.pcld.utils.elara;

import java.io.Serializable;
import java.util.List;


public class JournalData
        implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long journalCode;
    private String transactionCode;
    private String versionCode;
    private String countryCode;
    private List<AccountingTransactionData> accountingTransactionList;

    public String toString() {
        return "JournalData [accountingTransactionList=" + this.accountingTransactionList + ", countryCode=" + this.countryCode + ", journalCode=" + this.journalCode + ", transactionCode=" + this.transactionCode + ", versionCode=" + this.versionCode + "]";
    }


    public JournalData() {
    }


    public JournalData(Long journalCode, String transactionCode, String versionCode, String countryCode, List<AccountingTransactionData> accountingTransactionList) {
        this.journalCode = journalCode;
        this.transactionCode = transactionCode;
        this.versionCode = versionCode;
        this.countryCode = countryCode;
        this.accountingTransactionList = accountingTransactionList;
    }






    public Long getJournalCode() {
        return this.journalCode;
    }


    public void setJournalCode(Long journalCode) {
        this.journalCode = journalCode;
    }


    public String getTransactionCode() {
        return this.transactionCode;
    }


    public void setTransactionCode(String transactionCode) {
        this.transactionCode = transactionCode;
    }


    public String getVersionCode() {
        return this.versionCode;
    }


    public void setVersionCode(String versionCode) {
        this.versionCode = versionCode;
    }


    public String getCountryCode() {
        return this.countryCode;
    }


    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }


    public List<AccountingTransactionData> getAccountingTransactionList() {
        return this.accountingTransactionList;
    }


    public void setAccountingTransactionList(List<AccountingTransactionData> accountingTransactionList) {
        this.accountingTransactionList = accountingTransactionList;
    }
}
