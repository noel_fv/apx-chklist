package com.example.demo.pcld.utils.elara;

import java.io.Serializable;


public class ExternalData
        implements Serializable {
    private static final long serialVersionUID = 1L;
    private String targetType;
    private String targetID;
    private String message;

    public String toString() {
        return "ExternalData [message=" + this.message + ", targetID=" + this.targetID + ", targetType=" + this.targetType + "]";
    }


    public ExternalData() {
    }


    public ExternalData(String targetType, String targetID, String message) {
        this.targetType = targetType;
        this.targetID = targetID;
        this.message = message;
    }


    public final String getTargetType() {
        return this.targetType;
    }


    public final void setTargetType(String targetType) {
        this.targetType = targetType;
    }


    public final String getTargetID() {
        return this.targetID;
    }


    public final void setTargetID(String targetID) {
        this.targetID = targetID;
    }


    public final String getMessage() {
        return this.message;
    }


    public final void setMessage(String message) {
        this.message = message;
    }


}
