package com.example.demo.pcld.utils;


import com.example.demo.pcld.external.paomr001.dto.TbRulesVersion;

public class TbVersionMock {

    private static final TbVersionMock INSTANCE = new TbVersionMock();

    public static TbVersionMock getInstance() {
        return INSTANCE;
    }

    public TbRulesVersion getTbVersion()  {
        TbRulesVersion tbRulesVersion =new TbRulesVersion();
        tbRulesVersion.setNamespaceName("pe.pcld.documentarychecklist-dev");
        tbRulesVersion.setVersionName("0.0.3-SNAPSHOT");
        tbRulesVersion.setRuleName("ChecklistDinamico");
        return tbRulesVersion;
    }

}
