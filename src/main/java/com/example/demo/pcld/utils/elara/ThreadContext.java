package com.example.demo.pcld.utils.elara;






public class ThreadContext
{
    private static final ThreadLocal<Context> threadContext = new ThreadLocal();


    public static void set(Context context) { threadContext.set(context); }



    public static void unset() { threadContext.remove(); }



    public static Context get() { return (Context)threadContext.get(); }
}
