package com.example.demo.pcld.utils.elara;


import java.io.Serializable;


public class TransactionParameter
        implements Serializable {
    private static final long serialVersionUID = 1L;
    private String name;
    private Object value;

    public TransactionParameter() {
    }

    public TransactionParameter(String name, Object value) {
        this.name = name;
        this.value = value;
    }


    public final String getName() {
        return this.name;
    }


    public final void setName(String name) {
        this.name = name;
    }


    public final Object getValue() {
        return this.value;
    }


    public final void setValue(Object value) {
        this.value = value;
    }


    public final String toString() {
        return "Parameter [name=" + this.name + ", value=" + this.value + "]";
    }


    public int hashCode() {
        int prime = 31;
        int result = 1;
        result = 31 * result + ((this.name == null) ? 0 : this.name.hashCode());
        return 31 * result + ((this.value == null) ? 0 : this.value.hashCode());
    }


    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TransactionParameter other = (TransactionParameter) obj;
        if (this.name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!this.name.equals(other.name)) {
            return false;
        }
        if (this.value == null) {
            if (other.value != null) {
                return false;
            }
        } else if (!this.value.equals(other.value)) {
            return false;
        }
        return true;
    }
}
