package com.example.demo.pcld.utils.elara;

import java.io.Serializable;
import javax.sql.DataSource;


public class DataSourceInfo
        implements Serializable {
    private static final long serialVersionUID = 1L;
    private String username;
    private String password;
    private String url;
    private String driverClassName;
    private String jndiURL;
    private String connectionPoolInitial;
    private String connectionPoolMin;
    private String connectionPoolMax;
    private DataSource dataSource;
    private boolean showSql;
    private String typeDatabase;

    public DataSourceInfo(String jndiURL, String username, String password, String url, String driverClassName) {
        this.jndiURL = jndiURL;
        this.username = username;
        this.password = password;
        this.url = url;
        this.driverClassName = driverClassName;
    }

    public String getUsername() {
        return this.username;
    }


    public String getPassword() {
        return this.password;
    }


    public String getUrl() {
        return this.url;
    }


    public String getDriverClassName() {
        return this.driverClassName;
    }


    public DataSource getDataSource() {
        return this.dataSource;
    }


    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    public String getConnectionPoolInitial() {
        return this.connectionPoolInitial;
    }


    public void setConnectionPoolInitial(String connectionPoolInitial) {
        this.connectionPoolInitial = connectionPoolInitial;
    }


    public String getConnectionPoolMin() {
        return this.connectionPoolMin;
    }


    public void setConnectionPoolMin(String connectionPoolMin) {
        this.connectionPoolMin = connectionPoolMin;
    }


    public String getConnectionPoolMax() {
        return this.connectionPoolMax;
    }


    public void setConnectionPoolMax(String connectionPoolMax) {
        this.connectionPoolMax = connectionPoolMax;
    }


    public boolean isShowSql() {
        return this.showSql;
    }


    public void setShowSql(boolean showSql) {
        this.showSql = showSql;
    }


    public String getTypeDatabase() {
        return this.typeDatabase;
    }


    public void setTypeDatabase(String typeDatabase) {
        this.typeDatabase = typeDatabase;
    }


    public String getJndiURL() {
        return this.jndiURL;
    }


    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DataSourceInfo other = (DataSourceInfo) obj;
        if (this.connectionPoolInitial == null) {
            if (other.connectionPoolInitial != null)
                return false;
        } else if (!this.connectionPoolInitial.equals(other.connectionPoolInitial)) {
            return false;
        }
        if (this.connectionPoolMax == null) {
            if (other.connectionPoolMax != null)
                return false;
        } else if (!this.connectionPoolMax.equals(other.connectionPoolMax)) {
            return false;
        }
        if (this.connectionPoolMin == null) {
            if (other.connectionPoolMin != null)
                return false;
        } else if (!this.connectionPoolMin.equals(other.connectionPoolMin)) {
            return false;
        }
        if (this.dataSource == null) {
            if (other.dataSource != null)
                return false;
        } else if (!this.dataSource.equals(other.dataSource)) {
            return false;
        }
        if (this.driverClassName == null) {
            if (other.driverClassName != null)
                return false;
        } else if (!this.driverClassName.equals(other.driverClassName)) {
            return false;
        }
        if (this.jndiURL == null) {
            if (other.jndiURL != null)
                return false;
        } else if (!this.jndiURL.equals(other.jndiURL)) {
            return false;
        }
        if (this.password == null) {
            if (other.password != null)
                return false;
        } else if (!this.password.equals(other.password)) {
            return false;
        }
        if (this.showSql != other.showSql)
            return false;
        if (this.typeDatabase == null) {
            if (other.typeDatabase != null)
                return false;
        } else if (!this.typeDatabase.equals(other.typeDatabase)) {
            return false;
        }
        if (this.url == null) {
            if (other.url != null)
                return false;
        } else if (!this.url.equals(other.url)) {
            return false;
        }
        if (this.username == null) {
            if (other.username != null)
                return false;
        } else if (!this.username.equals(other.username)) {
            return false;
        }
        return true;
    }


    public int hashCode() {
        int prime = 31;
        int result = 1;


        result = 31 * result + ((this.connectionPoolInitial == null) ? 0 : this.connectionPoolInitial.hashCode());


        result = 31 * result + ((this.connectionPoolMax == null) ? 0 : this.connectionPoolMax.hashCode());


        result = 31 * result + ((this.connectionPoolMin == null) ? 0 : this.connectionPoolMin.hashCode());


        result = 31 * result + ((this.driverClassName == null) ? 0 : this.driverClassName.hashCode());

        result = 31 * result + ((this.jndiURL == null) ? 0 : this.jndiURL.hashCode());

        result = 31 * result + ((this.password == null) ? 0 : this.password.hashCode());

        result = 31 * result + ((this.url == null) ? 0 : this.url.hashCode());

        result = 31 * result + ((this.username == null) ? 0 : this.username.hashCode());


        return 31 * result + ((this.typeDatabase == null) ? 0 : this.typeDatabase.hashCode());
    }


    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("DataSourceInfo [username=");
        builder.append(this.username);
        builder.append(", password=");
        builder.append(this.password);
        builder.append(", jndiURL=");
        builder.append(this.jndiURL);
        builder.append(", url=");
        builder.append(this.url);
        builder.append(", driverClassName=");
        builder.append(this.driverClassName);
        builder.append(", connectionPoolInitial=");
        builder.append(this.connectionPoolInitial);
        builder.append(", connectionPoolMin=");
        builder.append(this.connectionPoolMin);
        builder.append(", connectionPoolMax=");
        builder.append(this.connectionPoolMax);
        builder.append(", showSql=");
        builder.append(this.showSql);
        builder.append(", typeDatabase=");
        builder.append(this.typeDatabase);
        builder.append("]");
        return builder.toString();
    }
}
