package com.example.demo.pcld.utils.elara;
import java.util.List;
import java.util.Map;

public interface CommonJdbcTemplate {
    int queryForInt(String paramString, Map<String, ?> paramMap);

    int queryForInt(String paramString, Object... paramVarArgs);

    long queryForLong(String paramString, Map<String, ?> paramMap);

    long queryForLong(String paramString, Object... paramVarArgs);

    <T> T queryForObject(String paramString, Class<T> paramClass, Map<String, ?> paramMap);

    <T> T queryForObject(String paramString, Class<T> paramClass, Object... paramVarArgs);

    Map<String, Object> queryForMap(String paramString, Map<String, ?> paramMap);

    Map<String, Object> queryForMap(String paramString, Object... paramVarArgs);

    List<Map<String, Object>> queryForList(String paramString, Map<String, ?> paramMap);

    List<Map<String, Object>> queryForList(String paramString, Object... paramVarArgs);

    int update(String paramString, Map<String, ?> paramMap);

    int update(String paramString, Object... paramVarArgs);

    int[] batchUpdate(String paramString, Map<String, ?>[] paramArrayOfMap);

    int[] batchUpdate(String paramString, List<Object[]> paramList);

    int[] batchUpdate(String paramString, List<Object[]> paramList, int[] paramArrayOfInt);
}
