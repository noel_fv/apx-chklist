package com.example.demo.pcld.utils.elara;

public   enum ContextStatusType {
    OK("0"),


    WARNING("1"),


    ERROR("2"),


    PGOK("00"),


    PGWARN("04"),


    PGREJ1("06"),


    PGREJ2("08"),


    PGERROR("12");


    private String value;


    ContextStatusType(String value) {
        this.value = value;
    }


    public String getValue() {
        return this.value;
    }
}
