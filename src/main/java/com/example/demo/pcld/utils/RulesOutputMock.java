package com.example.demo.pcld.utils;

import com.example.demo.pcld.libraries.lib.r005.dtos.output.OutputRules;
import com.fasterxml.jackson.core.JsonProcessingException;
//import org.codehaus.jackson.map.DeserializationConfig;
//import org.codehaus.jackson.map.ObjectMapper;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.IOException;
import java.io.InputStream;


public class RulesOutputMock {

	private static final RulesOutputMock INSTANCE = new RulesOutputMock();

	private ObjectMapper objectMapper;

	private RulesOutputMock() {
		objectMapper = new ObjectMapper();
		objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
		//objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
	}

	public static RulesOutputMock getInstance() {
		return INSTANCE;
	}

	public OutputRules getOutputRules() throws IOException {
		try  {
			InputStream is = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("mock/responseRulesV2.json");

			return objectMapper.readValue(is, OutputRules.class);
		}catch (IOException e){
			throw e;
		}
	}

	  public String writeValueAsString(OutputRules outputRules) throws JsonProcessingException {
		return objectMapper.writeValueAsString(outputRules);
	}

	public static void main(String[] args) throws Exception{
		OutputRules input= getInstance().getOutputRules();
		System.out.println(getInstance().writeValueAsString(input));

	}

}
