package com.example.demo.pcld.utils.elara;

import java.util.List;
import java.util.Map;

public interface JdbcUtils {
    int[] batchUpdate(String paramString, List<Object[]> paramList);

    int[] batchUpdate(String paramString, List<Object[]> paramList, int[] paramArrayOfInt);

    int queryForInt(String paramString, Object... paramVarArgs);

    int queryForInt(String paramString, Map<String, ?> paramMap);

    List<Map<String, Object>> queryForList(String paramString, Object... paramVarArgs);

    List<Map<String, Object>> pagingQueryForList(String paramString, int paramInt1, int paramInt2, Object... paramVarArgs);

    List<Map<String, Object>> queryForList(String paramString);

    List<Map<String, Object>> pagingQueryForList(String paramString, int paramInt1, int paramInt2);

    List<Map<String, Object>> queryForList(String paramString, Map<String, ?> paramMap);

    List<Map<String, Object>> pagingQueryForList(String paramString, int paramInt1, int paramInt2, Map<String, ?> paramMap);

    ParameterTable queryForTable(String paramString, Map<String, ?> paramMap);

    ParameterTable pagingQueryForTable(String paramString, int paramInt1, int paramInt2, Map<String, ?> paramMap);

    ParameterTable queryForTable(String paramString, Object... paramVarArgs);

    ParameterTable pagingQueryForTable(String paramString, int paramInt1, int paramInt2, Object... paramVarArgs);

    String queryForString(String paramString, Map<String, ?> paramMap);

    String queryForString(String paramString, Object... paramVarArgs);

    long queryForLong(String paramString, Map<String, ?> paramMap);

    long queryForLong(String paramString, Object... paramVarArgs);

    Map<String, Object> queryForMap(String paramString, Map<String, ?> paramMap);

    Map<String, Object> pagingQueryForMap(String paramString, int paramInt1, int paramInt2, Map<String, ?> paramMap);

    Map<String, Object> queryForMap(String paramString, Object... paramVarArgs);

    Map<String, Object> pagingQueryForMap(String paramString, int paramInt1, int paramInt2, Object... paramVarArgs);

    int[] batchUpdate(String paramString, Map<String, ?>[] paramArrayOfMap);

    int update(String paramString, Map<String, Object> paramMap);

    int update(String paramString, Object... paramVarArgs);

    int updateLob(String paramString, Map<String, Object> paramMap);

    int updateLob(String paramString, Object... paramVarArgs);

    void addQuery(String paramString1, String paramString2);

    String getQuery(String paramString);
}

