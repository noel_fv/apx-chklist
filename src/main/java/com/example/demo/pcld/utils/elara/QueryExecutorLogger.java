package com.example.demo.pcld.utils.elara;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class QueryExecutorLogger {
    private static final Logger LOGGER = LoggerFactory.getLogger(QueryExecutorLogger.class);


    private static final void traceParameter(String query, String key, Object value) {
        if (value == null) {
            LOGGER.debug("[SQL-" + query + "] Parameter: " + key + " = isNull");
        } else {
            LOGGER.debug("[SQL-" + query + "] Parameter: " + key + " = " + value);
        }
    }


    private static final void traceParameter(String query, Object value) {
        if (value == null) {
            LOGGER.debug("[SQL-" + query + "] Parameter: value = isNull");
        } else {
            LOGGER.debug("[SQL-" + query + "] Parameter: value = " + value);
        }
    }


    public static final void traceQuerySQL(String queryCode, String sql) {
        LOGGER.debug("[SQL-" + queryCode + "] SQL: query = " + sql);
    }


    public static final void traceQuery(String queryCode, Map<String, ?> args) {
        if (args != null &&
                LOGGER.isDebugEnabled()) {
            for (Map.Entry<String, ?> entry : args.entrySet()) {
                traceParameter(queryCode, (String) entry.getKey(), entry.getValue());
            }
        }
    }


    public static final void traceQuery(String queryCode, Object... args) {
        if (args != null &&
                LOGGER.isDebugEnabled())
            for (int i = 0; i < args.length; i++) {
                Object arg = args[i];
                traceParameter(queryCode, arg);
            }
    }
}
