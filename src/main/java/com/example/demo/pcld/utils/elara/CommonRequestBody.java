package com.example.demo.pcld.utils.elara;


import java.io.Serializable;
import java.util.List;


public class CommonRequestBody
        implements Serializable {
    private static final long serialVersionUID = 1L;
    private List<TransactionParameter> transactionParameters;

    public CommonRequestBody() {
    }

    public CommonRequestBody(List<TransactionParameter> transactionParameters) {
        this.transactionParameters = transactionParameters;
    }


    public final List<TransactionParameter> getTransactionParameters() {
        return this.transactionParameters;
    }


    public final void setTransactionParameters(List<TransactionParameter> transactionParameters) {
        this.transactionParameters = transactionParameters;
    }


    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("GenericBody [transactionParameters=");
        if (this.transactionParameters != null) {
            for (int i = 0; i < this.transactionParameters.size(); i++) {
                builder.append(((TransactionParameter) this.transactionParameters.get(i)).toString());
            }
        }
        builder.append("]");
        return builder.toString();
    }
}
