package com.example.demo.pcld.utils.elara;

import java.util.HashMap;
import java.util.Map;


public class ExtendedParams {
    private Map<String, Object> extendedParamsMap = new HashMap();


    public void putExtendedParam(String key, Object value) {
        this.extendedParamsMap.put(key, value);
    }


    public Object getExtendedParam(String key) {
        return this.extendedParamsMap.get(key);
    }


    public Map<String, Object> getExtendedParamsMap() {
        return this.extendedParamsMap;
    }
}
