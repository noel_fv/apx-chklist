package com.example.demo.pcld.utils.elara;


import java.io.Serializable;


public class EventData
        implements Serializable {
    private static final long serialVersionUID = 1L;
    private ExtendedParams extendedParams;

    public ExtendedParams getExtendedParams() {
        return this.extendedParams;
    }


    public void setExtendedParams(ExtendedParams extendedParams) {
        this.extendedParams = extendedParams;
    }
}
