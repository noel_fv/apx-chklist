package com.example.demo.pcld.utils.elara;


public   enum AdviceType {
    W("W"),


    E("E"),


    D("D");


    private String value;


    AdviceType(String value) {
        this.value = value;
    }


    public String getValue() {
        return this.value;
    }
}
