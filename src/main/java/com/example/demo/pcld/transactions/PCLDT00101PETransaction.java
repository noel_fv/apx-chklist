package com.example.demo.pcld.transactions;

import com.example.demo.pcld.utils.InputChekListMock;
import com.example.demo.pcld.dtos.input.ChecklistInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PCLDT00101PETransaction  extends AbstractPCLDT00101PETransaction{

    private static final Logger LOGGER = LoggerFactory.getLogger("PCLDT00101PETransaction");


    public  void execute() {
        try {

            this.inputChekList= InputChekListMock.getInstance().getInputChekList();
            ChecklistInfo checklistInfo=inputChekList.getChecklistInfo();

            System.out.println(checklistInfo.toString());
        }catch (Exception e){

        }

    }
}
