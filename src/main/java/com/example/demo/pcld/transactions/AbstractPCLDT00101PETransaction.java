package com.example.demo.pcld.transactions;

import com.example.demo.pcld.dtos.input.InputChekList;
import com.example.demo.pcld.dtos.output.OutputChekList;

public class AbstractPCLDT00101PETransaction {

    protected InputChekList inputChekList;
    protected OutputChekList outputChekList;
    protected String httpResponseCode;
    protected String severity;

    public InputChekList getInputChekList() {
        return inputChekList;
    }

    public void setInputChekList(InputChekList inputChekList) {
        this.inputChekList = inputChekList;
    }

    public OutputChekList getOutputChekList() {
        return outputChekList;
    }

    public void setOutputChekList(OutputChekList outputChekList) {
        this.outputChekList = outputChekList;
    }

    public String getHttpResponseCode() {
        return httpResponseCode;
    }

    public void setHttpResponseCode(String httpResponseCode) {
        this.httpResponseCode = httpResponseCode;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }
}
