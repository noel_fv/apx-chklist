package com.example.demo.pcld.dtos.common;

import java.io.Serializable;

public class Metadata implements Serializable {
    

   private Field field;

   public Metadata(Field field) {
      this.field = field;
   }

   public Metadata() {
   }

   public Field getField() {
      return field;
   }

   public void setField(Field field) {
      this.field = field;
   }
}