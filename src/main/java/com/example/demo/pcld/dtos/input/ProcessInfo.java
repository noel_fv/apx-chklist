package com.example.demo.pcld.dtos.input;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ProcessInfo implements Serializable {

	private static final long serialVersionUID = 6510226696263862403L;

	private String product;
	private String subproduct;
	@JsonProperty("id")
	private String id;
	private String stage;
	private String numberId;

	public ProcessInfo() {
	}



	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getSubproduct() {
		return subproduct;
	}

	public void setSubproduct(String subproduct) {
		this.subproduct = subproduct;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	public String getNumberId() {
		return numberId;
	}

	public void setNumberId(String numberId) {
		this.numberId = numberId;
	}
}
