package com.example.demo.pcld.dtos.output;

public class OutputChekList {

    private Data data;

    public OutputChekList() {
    }

    public OutputChekList(Data data) {
        this.data = data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Data getData() {
        return data;
    }
}
