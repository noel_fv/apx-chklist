package com.example.demo.pcld.dtos.input;


import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

public class ChecklistInfo implements Serializable {

	private static final long serialVersionUID = 8396614951376960252L;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date date;
	private Boolean reusability;

	public ChecklistInfo() {
		super();
	}

	public ChecklistInfo(Date date, Boolean reusability) {
		this.date = date;
		this.reusability = reusability;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Boolean getReusability() {
		return reusability;
	}

	public void setReusability(Boolean reusability) {
		this.reusability = reusability;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("checklistInfo: {\"date\":\"");
		builder.append(date);
		builder.append("\", \"reusability\":");
		builder.append(reusability);
		builder.append("}");
		return builder.toString();
	}

}
