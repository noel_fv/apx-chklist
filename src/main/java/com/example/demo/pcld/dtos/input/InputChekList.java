package com.example.demo.pcld.dtos.input;

import com.example.demo.pcld.dtos.common.Metadata;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class InputChekList implements Serializable {

    private ChecklistInfo checklistInfo;
    private ProcessInfo processInfo;
    private Participant participant;
    @JsonProperty("metadata")
    private List<Metadata> listMetadata;

    public InputChekList() {
    }

    public InputChekList(ChecklistInfo checklistInfo, ProcessInfo processInfo, Participant participant) {
        this.checklistInfo = checklistInfo;
        this.processInfo = processInfo;
        this.participant = participant;
    }

    public InputChekList(ChecklistInfo checklistInfo, ProcessInfo processInfo, Participant participant, List<Metadata> listMetadata) {
        this.checklistInfo = checklistInfo;
        this.processInfo = processInfo;
        this.participant = participant;
        this.listMetadata = listMetadata;
    }

    public List<Metadata> getListMetadata() {
        return listMetadata;
    }

    public void setListMetadata(List<Metadata> listMetadata) {
        this.listMetadata = listMetadata;
    }

    public ChecklistInfo getChecklistInfo() {
        return checklistInfo;
    }

    public void setChecklistInfo(ChecklistInfo checklistInfo) {
        this.checklistInfo = checklistInfo;
    }

    public ProcessInfo getProcessInfo() {
        return processInfo;
    }

    public void setProcessInfo(ProcessInfo processInfo) {
        this.processInfo = processInfo;
    }

    public Participant getParticipant() {
        return participant;
    }

    public void setParticipant(Participant participant) {
        this.participant = participant;
    }
}
