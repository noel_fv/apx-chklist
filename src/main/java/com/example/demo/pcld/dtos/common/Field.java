package com.example.demo.pcld.dtos.common;

import java.io.Serializable;

public class Field implements Serializable {


    private String name;
    private String type;
    private String value;


    public Field() {
    }

    public Field(String name, String type, String value) {
        this.name = name;
        this.type = type;
        this.value = value;
    }

    public Field(String name, String value) {
        this.name = name;
        this.type = "String";
        this.value = value;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    @Override
    public String toString() {
        return "Field{" +
                "name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}