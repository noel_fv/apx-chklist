
package com.example.demo.pcld.dtos.input;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;


public class DocumentType implements Serializable {

    @JsonProperty("id")
    private String id;
    private String documentNumber;

    public DocumentType(String id, String documentNumber) {
        this.id = id;
        this.documentNumber = documentNumber;
    }

    public DocumentType() {
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }
}
