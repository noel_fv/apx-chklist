package com.example.demo.pcld.dtos.output;


import java.io.Serializable;

public class Data implements Serializable {

    private Document document;

    public Data() {
    }

    public Data(Document document) {
        this.document = document;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }
}