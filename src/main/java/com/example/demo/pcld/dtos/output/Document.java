package com.example.demo.pcld.dtos.output;

import com.example.demo.pcld.dtos.common.Metadata;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Document {

    private String documentTypeId;
    private String name;
    private String description;
    private Boolean required;
    private String fileId;

    @JsonProperty("metadata")
    private List<Metadata> listMetadata;

    public Document() {
    }

    public Document(String documentTypeId, String name, String description, Boolean required, String fileId, List<Metadata> listMetadata) {
        this.documentTypeId = documentTypeId;
        this.name = name;
        this.description = description;
        this.required = required;
        this.fileId = fileId;
        this.listMetadata = listMetadata;
    }

    public String getDocumentTypeId() {
        return documentTypeId;
    }

    public void setDocumentTypeId(String documentTypeId) {
        this.documentTypeId = documentTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public List<Metadata> getListMetadata() {
        return listMetadata;
    }

    public void setListMetadata(List<Metadata> listMetadata) {
        this.listMetadata = listMetadata;
    }
}
