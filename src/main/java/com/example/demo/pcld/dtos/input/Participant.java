
package com.example.demo.pcld.dtos.input;


import java.io.Serializable;

public class Participant implements Serializable {


	private IdentityDocument identityDocument;

	public Participant() {
	}

	public Participant(IdentityDocument identityDocument) {
		this.identityDocument = identityDocument;
	}

	public IdentityDocument getIdentityDocument() {
		return identityDocument;
	}
}
