
package com.example.demo.pcld.dtos.input;


import java.io.Serializable;

public class IdentityDocument implements Serializable {

	private DocumentType documentType;

	public IdentityDocument() {
	}

	public IdentityDocument(DocumentType documentType) {
		this.documentType = documentType;
	}

	public DocumentType getDocumentType() {
		return documentType;
	}
}
