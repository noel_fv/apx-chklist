package com.example.demo.pcld.external.paomr001.lib.impl;

import com.example.demo.pcld.external.paomr001.dto.TbRulesVersion;
import com.example.demo.pcld.external.paomr001.lib.PAOMR001;
import com.example.demo.pcld.utils.TbVersionMock;
import com.example.demo.pcld.utils.elara.JdbcUtils;
import com.example.demo.pcld.utils.elara.JdbcUtilsImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.persistence.NoResultException;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class PAOMR001Impl  implements PAOMR001 {

    private static final Logger LOGGER = LoggerFactory.getLogger(PAOMR001Impl.class);
    private static final String TRACE_PCLDR001_IMPL = "[PCLD][PAOMR001Impl] - %s";
    JdbcUtils jdbcUtils=new JdbcUtilsImpl();

    @Override
    public List<Map<String, Object>> executeGetVersionInformation() {
        return null;
    }

    @Override
    public TbRulesVersion executeGetVersionBetweenDate(Date startDate, Date endDate) {

        LOGGER.info(String.format(TRACE_PCLDR001_IMPL, "executeGetVersionBetweenDate START"));
        TbRulesVersion tbRulesVersion =new TbRulesVersion();
        try {
            Map<String, Object> result = this.jdbcUtils.queryForMap("PAOM.readVersionData");
            //tbRulesVersion = TbVersionMapper.toMapper(result);
            tbRulesVersion = TbVersionMock.getInstance().getTbVersion();
            LOGGER.info(String.format(TRACE_PCLDR001_IMPL, "Result: " + tbRulesVersion.toString()));
            LOGGER.info(String.format(TRACE_PCLDR001_IMPL, "executeGetVersionBetweenDate END"));
        } catch (NoResultException e) {
            LOGGER.info("executeGetVersionBetweenDate - TbVersion NOT FOUND...");
        }
        return tbRulesVersion;

    }
}
