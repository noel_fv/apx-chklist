package com.example.demo.pcld.external.paomr001.lib;

import com.example.demo.pcld.external.paomr001.dto.TbRulesVersion;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface PAOMR001 {

    List<Map<String, Object>> executeGetVersionInformation();

    TbRulesVersion executeGetVersionBetweenDate(Date startDate, Date endDate);
}
