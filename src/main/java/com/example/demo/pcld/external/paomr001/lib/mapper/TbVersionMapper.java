package com.example.demo.pcld.external.paomr001.lib.mapper;

import com.example.demo.pcld.external.paomr001.dto.TbRulesVersion;

import java.util.Map;

public class TbVersionMapper {

    public static TbRulesVersion toMapper(Map<String, Object> result) {
        TbRulesVersion tbRulesVersion = new TbRulesVersion();
        tbRulesVersion.setRuleVersionId(Long.parseLong(result.get("RULE_VERSION_ID").toString()));
        tbRulesVersion.setRuleName(result.get("RULE_NAME").toString());
        tbRulesVersion.setVersionName(result.get("VERSION_NAME").toString());
        tbRulesVersion.setNamespaceName(result.get("NAMESPACE_NAME").toString());
        //tbVersion.setStartDate(result.get("START_DATE"));
        //tbVersion.setEndDate(result.get("END_DATE"));
        return tbRulesVersion;
    }
}
