package com.example.demo.pcld.external.paomr002.dto;

public class TbDocumentCatalog {

    private Long id;
    private String localDocumentTypeId;
    private String globalDocumentTypeId;
    private String documentTypeDesc;
    private Long reusabilityType;
    private String documentTypeDetailDesc;
    private Long valuedType;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLocalDocumentTypeId() {
        return localDocumentTypeId;
    }

    public void setLocalDocumentTypeId(String localDocumentTypeId) {
        this.localDocumentTypeId = localDocumentTypeId;
    }

    public String getGlobalDocumentTypeId() {
        return globalDocumentTypeId;
    }

    public void setGlobalDocumentTypeId(String globalDocumentTypeId) {
        this.globalDocumentTypeId = globalDocumentTypeId;
    }

    public String getDocumentTypeDesc() {
        return documentTypeDesc;
    }

    public void setDocumentTypeDesc(String documentTypeDesc) {
        this.documentTypeDesc = documentTypeDesc;
    }

    public Long getReusabilityType() {
        return reusabilityType;
    }

    public void setReusabilityType(Long reusabilityType) {
        this.reusabilityType = reusabilityType;
    }

    public String getDocumentTypeDetailDesc() {
        return documentTypeDetailDesc;
    }

    public void setDocumentTypeDetailDesc(String documentTypeDetailDesc) {
        this.documentTypeDetailDesc = documentTypeDetailDesc;
    }

    public Long getValuedType() {
        return valuedType;
    }

    public void setValuedType(Long valuedType) {
        this.valuedType = valuedType;
    }
}
