package com.example.demo.pcld.external.paomr003.lib.impl;

import com.example.demo.pcld.external.paomr003.lib.PAOMR003;
import com.example.demo.pcld.utils.elara.JdbcUtils;
import com.example.demo.pcld.utils.elara.JdbcUtilsImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;

import java.util.Map;

public class PAOMR003Impl implements PAOMR003 {

    private static final String ERROR_GET_INSERT_DATA = "PAOM00000002";
    private static final String TRACE_PCLDR003_IMPL = "[PCLD][PAOMR003Impl] - %s";
    private static final Logger LOGGER = LoggerFactory.getLogger(PAOMR003Impl.class);
    JdbcUtils jdbcUtils=new JdbcUtilsImpl();

    @Override
    public int executeInsertTrazability(Map<String, Object> mapIn) {

        try {
            LOGGER.info(String.format(TRACE_PCLDR003_IMPL, "executeInsertTrazability START"));
            int updatedNumber = this.jdbcUtils.update("PAOM.insertTrazabilidadData", mapIn);
            LOGGER.info(String.format(TRACE_PCLDR003_IMPL, "Result: " + updatedNumber));
            LOGGER.info(String.format(TRACE_PCLDR003_IMPL, "executeInsertTrazability END"));
            return updatedNumber;
        } catch (DuplicateKeyException e) {
            LOGGER.error(String.format(TRACE_PCLDR003_IMPL, " Error executeInsertTrazability: " + e.getMessage()));
            return 0;
        }

    }
}
