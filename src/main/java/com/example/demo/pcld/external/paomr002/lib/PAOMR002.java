package com.example.demo.pcld.external.paomr002.lib;

import java.util.List;
import java.util.Map;


public interface PAOMR002 {

    List<Map<String,Object>> executeGetCatalogoInformation(List<String> listDocumentTypeCode);

}
