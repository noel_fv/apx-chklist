package com.example.demo.pcld.external.paomr002.lib.mapper;

import com.example.demo.pcld.external.paomr002.dto.TbDocumentCatalog;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TbCatalogoMapper {


    public static TbDocumentCatalog toMapper(Map<String, Object> result) {
        TbDocumentCatalog tbDocumentCatalog = new TbDocumentCatalog();
        tbDocumentCatalog.setId(Long.parseLong(result.get("ID").toString()));
        tbDocumentCatalog.setLocalDocumentTypeId(result.get("LOCAL_DOCUMENT_TYPE_ID").toString());
        tbDocumentCatalog.setGlobalDocumentTypeId(result.get("GLOBAL_DOCUMENT_TYPE_ID").toString());
        tbDocumentCatalog.setDocumentTypeDesc(result.get("DOCUMENT_TYPE_DESC").toString());
        tbDocumentCatalog.setReusabilityType(Long.parseLong(result.get("REUSABILITY_TYPE").toString()));
        tbDocumentCatalog.setDocumentTypeDetailDesc(result.get("DOCUMENT_TYPE_DETAIL_DESC").toString());
        tbDocumentCatalog.setValuedType(Long.parseLong(result.get("VALUED_TYPE").toString()));
        return tbDocumentCatalog;
    }

    public static List<TbDocumentCatalog> toMappers(List<Map<String, Object>> results) {
        List<TbDocumentCatalog> listTbDocumentCatalogs = new ArrayList<>();

        results.forEach(result -> {
            listTbDocumentCatalogs.add(toMapper(result));
        });

        return listTbDocumentCatalogs;
    }



    public static Map<String, Object> requestListTbCatalogoToMapBd(List<String> listDocumentTypeCode) {
        Map<String, Object> query = new HashMap<>();
        query.put("LOCAL_DOCUMENT_TYPE_ID", listDocumentTypeCode.get(0));
        query.put("GLOBAL_DOCUMENT_TYPE_ID", listDocumentTypeCode.get(0));
        return query;
    }








}
