package com.example.demo.pcld.external.paomr003.lib.mapper;

import com.example.demo.pcld.external.paomr003.dto.TbTrazabilityCheckList;
import java.util.Map;

public class TbTrazabilityCheckListMapper {

    public static TbTrazabilityCheckList toMapper(Map<String, Object> result) {
        TbTrazabilityCheckList tbTrazabilityCheckList = new TbTrazabilityCheckList();
        tbTrazabilityCheckList.setChannelId(result.get("CHANNEL_ID").toString());
        tbTrazabilityCheckList.setProcessId(result.get("PROCESS_ID").toString());
        tbTrazabilityCheckList.setProcessStageName(result.get("PROCESS_STAGE_NAME").toString());
        tbTrazabilityCheckList.setProcessNumberId(result.get("PROCESS_NUMBER_ID").toString());
        tbTrazabilityCheckList.setParticipantIdentifierTypeId(result.get("PARTICIPANT_IDENTIFIER_TYPE_ID").toString());
        tbTrazabilityCheckList.setParticipantIdentifierId(result.get("PARTICIPANT_IDENTIFIER_ID").toString());
        tbTrazabilityCheckList.setRequestDate(result.get("REQUEST_DATE").toString());
        tbTrazabilityCheckList.setCreationUserId(result.get("CREATION_USER_ID").toString());
        tbTrazabilityCheckList.setCreationDate(result.get("CREATION_DATE").toString());
        tbTrazabilityCheckList.setUserAuditId(result.get("USER_AUDIT_ID").toString());
        tbTrazabilityCheckList.setAuditDate(result.get("AUDIT_DATE").toString());
        return tbTrazabilityCheckList;
    }







}
