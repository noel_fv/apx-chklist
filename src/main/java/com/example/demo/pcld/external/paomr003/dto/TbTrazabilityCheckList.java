package com.example.demo.pcld.external.paomr003.dto;

public class TbTrazabilityCheckList {

    private Long trazabilityChecklistSeqId;
    private String channelId;
    private String processId;
    private String processStageName;
    private String processNumberId;
    private String participantIdentifierTypeId;
    private String participantIdentifierId;
    private String requestDate;
    private String creationUserId;
    private String creationDate;
    private String userAuditId;
    private String auditDate;

    public Long getTrazabilityChecklistSeqId() {
        return trazabilityChecklistSeqId;
    }

    public void setTrazabilityChecklistSeqId(Long trazabilityChecklistSeqId) {
        this.trazabilityChecklistSeqId = trazabilityChecklistSeqId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getProcessStageName() {
        return processStageName;
    }

    public void setProcessStageName(String processStageName) {
        this.processStageName = processStageName;
    }

    public String getProcessNumberId() {
        return processNumberId;
    }

    public void setProcessNumberId(String processNumberId) {
        this.processNumberId = processNumberId;
    }

    public String getParticipantIdentifierTypeId() {
        return participantIdentifierTypeId;
    }

    public void setParticipantIdentifierTypeId(String participantIdentifierTypeId) {
        this.participantIdentifierTypeId = participantIdentifierTypeId;
    }

    public String getParticipantIdentifierId() {
        return participantIdentifierId;
    }

    public void setParticipantIdentifierId(String participantIdentifierId) {
        this.participantIdentifierId = participantIdentifierId;
    }

    public String getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(String requestDate) {
        this.requestDate = requestDate;
    }

    public String getCreationUserId() {
        return creationUserId;
    }

    public void setCreationUserId(String creationUserId) {
        this.creationUserId = creationUserId;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getUserAuditId() {
        return userAuditId;
    }

    public void setUserAuditId(String userAuditId) {
        this.userAuditId = userAuditId;
    }

    public String getAuditDate() {
        return auditDate;
    }

    public void setAuditDate(String auditDate) {
        this.auditDate = auditDate;
    }
}
