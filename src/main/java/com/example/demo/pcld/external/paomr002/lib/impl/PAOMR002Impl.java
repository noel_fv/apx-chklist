package com.example.demo.pcld.external.paomr002.lib.impl;

import com.example.demo.pcld.external.paomr002.lib.PAOMR002;
import com.example.demo.pcld.utils.elara.JdbcUtils;
import com.example.demo.pcld.utils.elara.JdbcUtilsImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.persistence.NoResultException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PAOMR002Impl implements PAOMR002 {

    private static final Logger LOGGER = LoggerFactory.getLogger(PAOMR002Impl.class);
    private static final String ERROR_GET_BASIC_DATA = "PAOM00000001";
    private static final String TRACE_PAOMR002_IMPL = " [PAOM][PAOMR002Impl] - %s";
    JdbcUtils jdbcUtils=new JdbcUtilsImpl();

    @Override
    public List<Map<String,Object>> executeGetCatalogoInformation(List<String> listDocumentTypeCode) {

        try {
            LOGGER.info(String.format(TRACE_PAOMR002_IMPL, "executeGetCatalogoInformation START"));
            Map<String, Object> argumentos = new HashMap<>();
            argumentos.put("SELECT_WHERE_IN", listDocumentTypeCode);
            print(listDocumentTypeCode);
            List<Map<String, Object>>  mapOut= this.jdbcUtils.queryForList("PAOM.readCatalogoData", listDocumentTypeCode);
            LOGGER.info(String.format(TRACE_PAOMR002_IMPL, "executeGetCatalogoInformation END"));
            return mapOut;
        } catch (NoResultException e) {
            LOGGER.error(String.format(TRACE_PAOMR002_IMPL, " Error executeGetCatalogoInformation: " + e.getMessage()));
            return Collections.emptyList();
        }
    }

    private void print(List<String> listDocumentTypeCode){
        for (int i = 0; i < listDocumentTypeCode.size(); i++) {
            System.out.println(listDocumentTypeCode.get(i));
        }
    }
}
