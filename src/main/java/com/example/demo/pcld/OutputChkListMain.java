package com.example.demo.pcld;

import com.example.demo.pcld.dtos.common.Field;
import com.example.demo.pcld.dtos.common.Metadata;
import com.example.demo.pcld.dtos.output.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class OutputChkListMain {


    public static void main(String[] args)throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);


        Data data=new Data();
        Document document=new Document();
        document.setDocumentTypeId("0001");
        document.setName("DNI");
        document.setDescription("Documento de Identidad");
        document.setRequired(true);
        document.setFileId("{50B01371-0000-CF1F-B21C-CE106EBCFADE}");
        List<Metadata> listMetadata=new ArrayList<>();
        listMetadata.add(new Metadata(new Field("TIPO_PERSONA","PNN")));
        listMetadata.add(new Metadata(new Field("PEP25","Boolean","true")));
        document.setListMetadata(listMetadata);
        data.setDocument(document);

        String postJson = mapper.writeValueAsString(new OutputChekList(data));
        System.out.println(postJson);

    }
}
