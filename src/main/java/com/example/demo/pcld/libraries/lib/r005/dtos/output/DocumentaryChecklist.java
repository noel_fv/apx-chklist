package com.example.demo.pcld.libraries.lib.r005.dtos.output;


import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DocumentaryChecklist implements Serializable {

    private String documentTypeCode;
    private String documentTypeDetail;
    private String status;
    private String condition;

    public DocumentaryChecklist() {
    }

    public DocumentaryChecklist(String documentTypeCode, String status) {
        this.documentTypeCode = documentTypeCode;
        this.status = status;
    }

    public DocumentaryChecklist(String documentTypeCode, String documentTypeDetail, String status, String condition) {
        this.documentTypeCode = documentTypeCode;
        this.documentTypeDetail = documentTypeDetail;
        this.status = status;
        this.condition = condition;
    }

    public String getDocumentTypeCode() {
        return documentTypeCode;
    }

    public void setDocumentTypeCode(String documentTypeCode) {
        this.documentTypeCode = documentTypeCode;
    }

    public String getDocumentTypeDetail() {
        return documentTypeDetail;
    }

    public void setDocumentTypeDetail(String documentTypeDetail) {
        this.documentTypeDetail = documentTypeDetail;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }
}
