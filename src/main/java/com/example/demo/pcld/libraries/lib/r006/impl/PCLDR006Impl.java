package com.example.demo.pcld.libraries.lib.r006.impl;


import com.example.demo.pcld.dtos.output.Data;
import com.example.demo.pcld.libraries.lib.r006.PCLDR006;
import com.example.demo.pcld.utils.APIConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

public class PCLDR006Impl implements PCLDR006 {

    private static final Logger LOGGER = LoggerFactory.getLogger("PCLDR006Impl");
    private static final String API_REUTILIZATION = "reutilization";
    private static final String TRACE_PCLDR006_IMPL = "[PCLD][PCLDR006Impl] - %s";

    APIConnector internalApiConnector=new APIConnector();

    @Override
    public Data executeGetApiReutilizacion(List<Map<String,Object>> listTbDocumentCatalog) {
        LOGGER.info(String.format(TRACE_PCLDR006_IMPL, "executeGetApiReutilizacion START"));
        //TODO invocacion al API Connector
        Data a = internalApiConnector.getForObject(API_REUTILIZATION, Data.class);
        LOGGER.info(String.format(TRACE_PCLDR006_IMPL, "executeGetApiReutilizacion END "));
        return a;
    }
}
