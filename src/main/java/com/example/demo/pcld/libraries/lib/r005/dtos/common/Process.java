
package com.example.demo.pcld.libraries.lib.r005.dtos.common;

import java.io.Serializable;

public class Process  implements Serializable {

    private String contractingCode;
    private String channelCode;
    private String processTypeCode;

    public Process() {
    }

    public String getContractingCode() {
        return contractingCode;
    }

    public void setContractingCode(String contractingCode) {
        this.contractingCode = contractingCode;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getProcessTypeCode() {
        return processTypeCode;
    }

    public void setProcessTypeCode(String processTypeCode) {
        this.processTypeCode = processTypeCode;
    }
}
