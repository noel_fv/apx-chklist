
package com.example.demo.pcld.libraries.lib.r005.dtos.common;


import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

public class Customer   implements Serializable {


    private Boolean pep;
    private Boolean fatca;
    @JsonProperty("new")
    private Boolean _new;
    private Boolean newDebitCard;
    private Boolean jointAccount;
    private Boolean digitalAccountStatement;


    public Boolean getPep() {
        return pep;
    }

    public void setPep(Boolean pep) {
        this.pep = pep;
    }

    public Boolean getFatca() {
        return fatca;
    }

    public void setFatca(Boolean fatca) {
        this.fatca = fatca;
    }

    public Boolean get_new() {
        return _new;
    }

    public void set_new(Boolean _new) {
        this._new = _new;
    }

    public Boolean getNewDebitCard() {
        return newDebitCard;
    }

    public void setNewDebitCard(Boolean newDebitCard) {
        this.newDebitCard = newDebitCard;
    }

    public Boolean getJointAccount() {
        return jointAccount;
    }

    public void setJointAccount(Boolean jointAccount) {
        this.jointAccount = jointAccount;
    }

    public Boolean getDigitalAccountStatement() {
        return digitalAccountStatement;
    }

    public void setDigitalAccountStatement(Boolean digitalAccountStatement) {
        this.digitalAccountStatement = digitalAccountStatement;
    }
}
