package com.example.demo.pcld.libraries.lib.r005.dtos.input;

import com.example.demo.pcld.libraries.lib.r005.dtos.common.Customer;
import com.example.demo.pcld.libraries.lib.r005.dtos.common.Process;
import com.example.demo.pcld.libraries.lib.r005.dtos.common.Product;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class InputRules implements Serializable  {


  /*  @JsonProperty("model-namespace")
    private String modelNameSpace;
    @JsonProperty("model-name")
    private String modelName;
    @JsonProperty("decision-name")
    private String decisionName;
    @JsonProperty("decision-id")
    private String decisionId;
    @JsonProperty("dmn-context")
    private DmnContextInput dmnContext;*/

    //Version 2
    @JsonProperty("Process")
    private Process process;
    @JsonProperty("Product")
    private Product product;
    @JsonProperty("Customer")
    private Customer customer;

    public InputRules() {
    }

    public InputRules(Process process, Product product, Customer customer) {
        this.process = process;
        this.product = product;
        this.customer = customer;
    }

    public Process getProcess() {
        return process;
    }

    public void setProcess(Process process) {
        this.process = process;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }



/*

    public String getModelNameSpace() {
        return modelNameSpace;
    }

    public void setModelNameSpace(String modelNameSpace) {
        this.modelNameSpace = modelNameSpace;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getDecisionName() {
        return decisionName;
    }

    public void setDecisionName(String decisionName) {
        this.decisionName = decisionName;
    }

    public String getDecisionId() {
        return decisionId;
    }

    public void setDecisionId(String decisionId) {
        this.decisionId = decisionId;
    }

    public DmnContextInput getDmnContext() {
        return dmnContext;
    }

    public void setDmnContext(DmnContextInput dmnContextInput) {
        this.dmnContext = dmnContextInput;
    }

 */
}
