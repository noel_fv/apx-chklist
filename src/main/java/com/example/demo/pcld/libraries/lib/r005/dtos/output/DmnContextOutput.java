package com.example.demo.pcld.libraries.lib.r005.dtos.output;

import com.example.demo.pcld.libraries.lib.r005.dtos.common.Customer;
import com.example.demo.pcld.libraries.lib.r005.dtos.common.Process;
import com.example.demo.pcld.libraries.lib.r005.dtos.common.Product;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class DmnContextOutput implements Serializable {

    @JsonProperty("Process")
    private Process process;
    @JsonProperty("Product")
    private Product product;
    @JsonProperty("Customer")
    private Customer customer;
    @JsonProperty("DocumentaryChecklist")
    private List<DocumentaryChecklist> listDocumentaryChecklist;


    public Process getProcess() {
        return process;
    }

    public void setProcess(Process process) {
        this.process = process;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<DocumentaryChecklist> getListDocumentaryChecklist() {
        return listDocumentaryChecklist;
    }

    public void setListDocumentaryChecklist(List<DocumentaryChecklist> listDocumentaryChecklist) {
        this.listDocumentaryChecklist = listDocumentaryChecklist;
    }
}
