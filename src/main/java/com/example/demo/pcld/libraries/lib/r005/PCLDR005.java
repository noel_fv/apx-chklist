package com.example.demo.pcld.libraries.lib.r005;

import com.example.demo.pcld.dtos.input.InputChekList;
import com.example.demo.pcld.external.paomr001.dto.TbRulesVersion;
import com.example.demo.pcld.libraries.lib.r005.dtos.output.OutputRules;

public interface PCLDR005 {

    OutputRules executeInvokeRules(TbRulesVersion tbRulesVersion, InputChekList inputChekList);

}
