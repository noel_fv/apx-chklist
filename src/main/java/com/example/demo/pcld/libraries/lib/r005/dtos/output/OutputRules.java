package com.example.demo.pcld.libraries.lib.r005.dtos.output;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.List;


public class OutputRules implements Serializable {

    /*
    @JsonProperty("messages")
    private List<String> messages;
    @JsonProperty("model-namespace")
    private String modelNameSpace;
    @JsonProperty("model-name")
    private String modelName;
    @JsonProperty("decision-name")
    private String decisionName;
    @JsonProperty("dmn-context")
    private DmnContextOutput dmnContext;*/

    @JsonProperty("DocumentaryChecklist")
    private List<DocumentaryChecklist> listDocumentaryChecklist;

    public OutputRules() {
    }

    public OutputRules(List<DocumentaryChecklist> listDocumentaryChecklist) {
        this.listDocumentaryChecklist = listDocumentaryChecklist;
    }

    public List<DocumentaryChecklist> getListDocumentaryChecklist() {
        return listDocumentaryChecklist;
    }

    public void setListDocumentaryChecklist(List<DocumentaryChecklist> listDocumentaryChecklist) {
        this.listDocumentaryChecklist = listDocumentaryChecklist;
    }

    /*
    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

    public String getModelNameSpace() {
        return modelNameSpace;
    }

    public void setModelNameSpace(String modelNameSpace) {
        this.modelNameSpace = modelNameSpace;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getDecisionName() {
        return decisionName;
    }

    public void setDecisionName(String decisionName) {
        this.decisionName = decisionName;
    }

    public DmnContextOutput getDmnContext() {
        return dmnContext;
    }

    public void setDmnContext(DmnContextOutput dmnContext) {
        this.dmnContext = dmnContext;
    }

     */
}
