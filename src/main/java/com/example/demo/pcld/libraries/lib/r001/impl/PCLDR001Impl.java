package com.example.demo.pcld.libraries.lib.r001.impl;


import com.example.demo.pcld.utils.CheckListOutputMock;
import com.example.demo.pcld.utils.InputChekListMock;
import com.example.demo.pcld.dtos.input.ChecklistInfo;
import com.example.demo.pcld.dtos.input.InputChekList;
import com.example.demo.pcld.dtos.output.Data;
import com.example.demo.pcld.dtos.output.OutputChekList;
import com.example.demo.pcld.external.paomr001.dto.TbRulesVersion;
import com.example.demo.pcld.external.paomr001.lib.PAOMR001;
import com.example.demo.pcld.external.paomr001.lib.impl.PAOMR001Impl;
import com.example.demo.pcld.external.paomr002.lib.PAOMR002;
import com.example.demo.pcld.external.paomr002.lib.impl.PAOMR002Impl;
import com.example.demo.pcld.external.paomr003.lib.PAOMR003;
import com.example.demo.pcld.external.paomr003.lib.impl.PAOMR003Impl;
import com.example.demo.pcld.libraries.lib.r001.PCLDR001;
import com.example.demo.pcld.libraries.lib.r005.PCLDR005;
import com.example.demo.pcld.libraries.lib.r005.dtos.output.OutputRules;
import com.example.demo.pcld.libraries.lib.r005.impl.PCLDR005Impl;
import com.example.demo.pcld.libraries.lib.r006.PCLDR006;
import com.example.demo.pcld.libraries.lib.r006.impl.PCLDR006Impl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class PCLDR001Impl implements PCLDR001 {

    private static final String PCLDR001Impl = "[PCLD][PCLDR001Impl] - %s";
    private static final Logger LOGGER = LoggerFactory.getLogger(PCLDR001Impl.class);

    @Override
    public OutputChekList execute(InputChekList inputChekList) {

        OutputChekList response;
        LOGGER.info(String.format(PCLDR001Impl, "execute START"));

        //Inicio de flujo Insertatrazabilidad
        PAOMR003 paomr003=new PAOMR003Impl();
        Map<String,Object> inputTrazabilidad=toMap(inputChekList);
        int res=paomr003.executeInsertTrazability(inputTrazabilidad);

        //TODO Consulta a la tabla version para obtener la URL de rules y namespace
        PAOMR001 paomr001=new PAOMR001Impl();
        ChecklistInfo checklistInfo=inputChekList.getChecklistInfo();
        TbRulesVersion tbRulesVersion =paomr001.executeGetVersionBetweenDate(checklistInfo.getDate(),checklistInfo.getDate());

        //TODO Invocando a RULES
        PCLDR005 pcldr005=new PCLDR005Impl();
        OutputRules outputRules=pcldr005.executeInvokeRules(tbRulesVersion,inputChekList);

        //TODO invoca a tabla catalogo
        PAOMR002 paomr002=new PAOMR002Impl();
        List<String> listDocumentTypeCode=toList(outputRules);
        List<Map<String,Object>> listTbDocumentCatalog=paomr002.executeGetCatalogoInformation(listDocumentTypeCode);
        //Falta implementar

        //TODO invocar a Gestor Docuemntal
        Boolean reusability=inputChekList.getChecklistInfo().getReusability();
        Data data;
        if(!reusability){ //Reusability es false no se debe envia la consulta al gestor documental(reutalizacion)
            //TODO Generar respuesta del checkList
            data=new Data();
            response= CheckListOutputMock.getInstance().getOutputChekList();
        }else {
            //TODO Generar respuesta del checkList con informacion del Gestor documental
            PCLDR006 pcldr006 = new PCLDR006Impl();
            data = pcldr006.executeGetApiReutilizacion(listTbDocumentCatalog);
            response=new OutputChekList(data);
        }

        LOGGER.info(String.format(PCLDR001Impl, "Generando chkList Respuesta "+CheckListOutputMock.getInstance().writeValueAsString(response)));
        LOGGER.info(String.format(PCLDR001Impl, "execute END"));
        return response;
    }




    public static void main(String[] args) throws Exception{
        InputChekList inputChekList= InputChekListMock.getInstance().getInputChekList();
        inputChekList.setChecklistInfo(new ChecklistInfo(new Date(),true));
        PCLDR001 lib=new PCLDR001Impl();
        lib.execute(inputChekList);
    }



    private Map<String,Object> toMap(InputChekList inputChekList){
        Map<String,Object> inputTrazability=new HashMap<>();
        inputTrazability.put("CHANNEL_ID","CHANNEL_ID");
        inputTrazability.put("PROCESS_ID",inputChekList.getProcessInfo().getId());
        inputTrazability.put("PROCESS_STAGE_NAME",inputChekList.getProcessInfo().getStage());
        inputTrazability.put("PROCESS_NUMBER_ID",inputChekList.getProcessInfo().getNumberId());
        inputTrazability.put("PARTICIPANT_IDENTIFIER_TYPE_ID",inputChekList.getParticipant().getIdentityDocument().getDocumentType());
        inputTrazability.put("PARTICIPANT_IDENTIFIER_ID",inputChekList.getParticipant().getIdentityDocument().getDocumentType().getId());
        inputTrazability.put("REQUEST_DATE",inputChekList.getChecklistInfo().getDate());
        inputTrazability.put("CREATION_USER_ID","XP0001");
        inputTrazability.put("CREATION_DATE","DATE.NOW()");
        inputTrazability.put("USER_AUDIT_ID","PAOM");
        inputTrazability.put("AUDIT_DATE","DATE.NOW()");
        return inputTrazability;
    }

    private List<String> toList(OutputRules outputRules){

        List<String> listDocumentTypeCode=new ArrayList<>();
        outputRules.getListDocumentaryChecklist().forEach(result -> {
            listDocumentTypeCode.add(result.getDocumentTypeCode());
        });
        //List<String> listDocumentTypeCode = outputRules.getListDocumentaryChecklist().stream().map(o -> o.getDocumentTypeCode()).collect(Collectors.toList());
        return listDocumentTypeCode;
    }


}
