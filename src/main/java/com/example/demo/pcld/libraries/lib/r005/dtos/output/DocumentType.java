
package com.example.demo.pcld.libraries.lib.r005.dtos.output;

import java.io.Serializable;


public class DocumentType implements Serializable {

    private String  documentTypeCode;
    private String  status;
    private String  condition;
    private String  documentTypeDetail;

    public String getDocumentTypeCode() {
        return documentTypeCode;
    }

    public void setDocumentTypeCode(String documentTypeCode) {
        this.documentTypeCode = documentTypeCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getDocumentTypeDetail() {
        return documentTypeDetail;
    }

    public void setDocumentTypeDetail(String documentTypeDetail) {
        this.documentTypeDetail = documentTypeDetail;
    }
}
