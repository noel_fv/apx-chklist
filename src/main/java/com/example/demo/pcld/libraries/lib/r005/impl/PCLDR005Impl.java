package com.example.demo.pcld.libraries.lib.r005.impl;

import com.example.demo.pcld.utils.RulesOutputMock;
import com.example.demo.pcld.dtos.input.InputChekList;
import com.example.demo.pcld.external.paomr001.dto.TbRulesVersion;
import com.example.demo.pcld.libraries.lib.r005.PCLDR005;
import com.example.demo.pcld.libraries.lib.r005.dtos.common.Customer;
import com.example.demo.pcld.libraries.lib.r005.dtos.common.Process;
import com.example.demo.pcld.libraries.lib.r005.dtos.common.Product;
import com.example.demo.pcld.libraries.lib.r005.dtos.input.InputRules;
import com.example.demo.pcld.libraries.lib.r005.dtos.output.OutputRules;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class PCLDR005Impl implements PCLDR005 {


    private static final Logger LOGGER = LoggerFactory.getLogger(PCLDR005Impl.class);
    private static final String TRACE_PCLDR005_IMPL = "[PCLD][PAOMR005Impl] - %s";
    private static final String APX_CONFIG_URL_RULES="https://rules.work-02.ether.igrupobbva/v0/";

    @Override
    public OutputRules executeInvokeRules(TbRulesVersion tbRulesVersion, InputChekList inputChekList) {

        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);

        try {
            LOGGER.info(String.format(TRACE_PCLDR005_IMPL, "executeInvokeRules START"));
            //Genera la URL de integracion con RULES
            String urlRulesService=createUrlRulesService(tbRulesVersion);
            LOGGER.info(String.format(TRACE_PCLDR005_IMPL, "URL RULES "+ urlRulesService));
            InputRules inputRules=createBodyInputRules(inputChekList);
            String postJsonRules= mapper.writeValueAsString(inputRules);
            LOGGER.info(String.format(TRACE_PCLDR005_IMPL, "Request a RULES"));
            LOGGER.info(String.format(TRACE_PCLDR005_IMPL, postJsonRules));
            //TODO invocacion al API Connector
            //OutputRules outputRules = apiConnector.execute(urlRulesService,inputRules);
            OutputRules outputRules = RulesOutputMock.getInstance().getOutputRules();
            String responseJsonRules= mapper.writeValueAsString(outputRules);

            LOGGER.info(String.format(TRACE_PCLDR005_IMPL, "Response a RULES"));
            LOGGER.info(String.format(TRACE_PCLDR005_IMPL, responseJsonRules));
            LOGGER.info(String.format(TRACE_PCLDR005_IMPL, "executeInvokeRules END"));
            return outputRules;

        }catch (Exception e){
            return  new OutputRules();
        }


    }

    public static String createUrlRulesService(TbRulesVersion tbRulesVersion) {
        StringBuilder sb = new StringBuilder(APX_CONFIG_URL_RULES);
        sb.append("ns/").append(tbRulesVersion.getNamespaceName()).append("/");
        sb.append("decisions/").append(tbRulesVersion.getRuleName()).append("/");
        sb.append("versions/").append(tbRulesVersion.getVersionName()).append("/");
        sb.append("decision-services/").append("DecisionService:evaluate");
        return sb.toString();
    }


    public static void main(String[] args) throws Exception{
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        TbRulesVersion tbRulesVersion =new TbRulesVersion();
        tbRulesVersion.setNamespaceName("pe.pcld.documentarychecklist-dev");
        tbRulesVersion.setVersionName("0.0.3-SNAPSHOT");
        tbRulesVersion.setRuleName("ChecklistDinamico");
        System.out.println(createUrlRulesService(tbRulesVersion));
        String postJson = mapper.writeValueAsString(createBodyInputRules(null));
        System.out.println(postJson);

    }




    public static InputRules createBodyInputRules( InputChekList inputChekList){

        Process process=new Process();
        process.setContractingCode("0058");//TODO Falta saber de donde sacar este dato
        process.setChannelCode("0002");//TODO Falta saber de donde sacar este dato
        process.setProcessTypeCode("REGULAR");//TODO Falta saber de donde sacar este dato
        Product product=new Product();
        //product.setProductId(inputChekList.getProcessInfo().getProduct());
        product.setProductId("01");
        Customer customer=new Customer();
        //TODO crear metodo convertidor de List<metadata> a customer
        //customer.setPep(inputChekList.getListMetadata().get(0).getField().getValue());
        customer.setPep(true);
        customer.setFatca(true);
        customer.set_new(true);
        customer.setNewDebitCard(true);
        customer.setJointAccount(true);
        customer.setDigitalAccountStatement(true);
        return new InputRules(process,product,customer);
    }


}

