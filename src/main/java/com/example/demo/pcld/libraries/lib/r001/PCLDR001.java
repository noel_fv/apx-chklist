package com.example.demo.pcld.libraries.lib.r001;

import com.example.demo.pcld.dtos.input.InputChekList;
import com.example.demo.pcld.dtos.output.OutputChekList;

public interface PCLDR001 {

    OutputChekList execute(InputChekList inputChekList);
}
