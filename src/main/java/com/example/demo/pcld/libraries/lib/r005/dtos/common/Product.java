
package com.example.demo.pcld.libraries.lib.r005.dtos.common;

import java.io.Serializable;


public class Product  implements Serializable {


    private String productId;

    public Product() {
    }

    public Product(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

}
