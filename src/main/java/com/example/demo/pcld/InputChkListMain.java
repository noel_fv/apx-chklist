package com.example.demo.pcld;

import com.example.demo.pcld.dtos.common.Field;
import com.example.demo.pcld.dtos.common.Metadata;
import com.example.demo.pcld.dtos.input.*;
import com.example.demo.pcld.utils.InputChekListMock;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

//import org.codehaus.jackson.map.DeserializationConfig;
//import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class InputChkListMain {

    static InputChekListMock chekListMock = InputChekListMock.getInstance();

    public static void main(String[] args)throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        //mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
        //mapper.enable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);



        ChecklistInfo checklistInfo=new ChecklistInfo(new Date(),true);
        ProcessInfo processInfo=new ProcessInfo();
        processInfo.setProduct("01");
        processInfo.setSubproduct("001");
        processInfo.setId("4.3.1.8_PE");
        processInfo.setStage("EVA");
        processInfo.setNumberId("FU190808766755");

        Participant participant=new Participant(new IdentityDocument(new DocumentType("R","FU190808766755")));
        List<Metadata> listMetadata=new ArrayList<>();
        listMetadata.add(new Metadata(new Field("TIPO_PERSONA","PNN")));
        listMetadata.add(new Metadata(new Field("PEP25","Boolean","true")));

        String postJson = mapper.writeValueAsString(new InputChekList(checklistInfo,processInfo,participant,listMetadata));
        System.out.println(postJson);

        InputChekList input= chekListMock.getInputChekList();
        System.out.println(input);
    }

    private static InputChekList mock() throws Exception{
        return chekListMock.getInputChekList();
    }

    static  void toMap(){

        try
        {
            InputChekList input= chekListMock.getInputChekList();
            ChecklistInfo checklistInfo=input.getChecklistInfo();
            ProcessInfo processInfo=input.getProcessInfo();
            Participant participant=input.getParticipant();
           // System.out.println(entityMock.writeValueAsString(input));

        }catch (Exception e) {

        }


    }
}
